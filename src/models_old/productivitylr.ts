import Knex, * as knex from 'knex';

export class ProductivitylrModel {

  constructor() { }

  select(db: knex) {
    return db('productivity_lr');
  }

  insert(db: knex,datas: any){
    return db('productivity_lr').insert(datas);
  }

  update(db: Knex,datas: any,id: any){
    return db('productivity_lr').update(datas).where('id',id);
  }

  delete(db: Knex,id: any){
    return db('productivity_lr').delete().where('id',id);
  }

  select_id(db: knex,id: any) {
    return db('productivity_lr').where('id',id);
  }

  select_date(db: Knex,datestart: any,dateend: any){
    return db('productivity_lr').whereBetween('vstdate',[datestart,dateend]);
  }

  async select_date_code(db: Knex,datestart: any,dateend: any,code: any){
    let sql:any = `select l.name,p.* from productivity_lr as p
    INNER JOIN l_dprtm as l on p.codewd = l.code where p.codewd = '${code}' and p.vstdate BETWEEN '${datestart}' and '${dateend}'
     order by p.vstdate,p.wer`;
    let data:any = await db.raw(sql);
    return data[0];
  }

  
  select_dprtm(db: knex,id: any){
    return db('l_dprtm').where('code',id);
  }
  //รายงานอัตรากำลังต่อวัน/เดือน/ปี
  async select_pd_year(db: knex,year: any,code: any){
    let sql:any =
    `SELECT day(vstdate) as cday,
    cast(sum(case month(vstdate) when 1 then cprodu  end)/3 as decimal(10,2)) as m1, 
    cast(sum(case month(vstdate) when 2 then cprodu  end)/3 as decimal(10,2)) as m2, 
    cast(sum(case month(vstdate) when 3 then cprodu  end)/3 as decimal(10,2)) as m3, 
    cast(sum(case month(vstdate) when 4 then cprodu  end)/3 as decimal(10,2)) as m4, 
    cast(sum(case month(vstdate) when 5 then cprodu  end)/3 as decimal(10,2)) as m5, 
    cast(sum(case month(vstdate) when 6 then cprodu  end)/3 as decimal(10,2)) as m6, 
    cast(sum(case month(vstdate) when 7 then cprodu  end)/3 as decimal(10,2)) as m7, 
    cast(sum(case month(vstdate) when 8 then cprodu  end)/3 as decimal(10,2)) as m8, 
    cast(sum(case month(vstdate) when 9 then cprodu  end)/3 as decimal(10,2)) as m9, 
    cast(sum(case month(vstdate) when 10 then cprodu  end)/3 as decimal(10,2)) as m10, 
    cast(sum(case month(vstdate) when 11 then cprodu  end)/3 as decimal(10,2)) as m11, 
    cast(sum(case month(vstdate) when 12 then cprodu  end)/3 as decimal(10,2)) as m12 
     from productivity_lr where year(vstdate) = '${year}' GROUP BY cday 
     union 
    select 'total', 
    cast(sum(case month(vstdate) when 1 then cprodu  end)/3 as decimal(10,2)) as m1,
    cast(sum(case month(vstdate) when 2 then cprodu  end)/3 as decimal(10,2)) as m2,
    cast(sum(case month(vstdate) when 3 then cprodu  end)/3 as decimal(10,2)) as m3,
    cast(sum(case month(vstdate) when 4 then cprodu  end)/3 as decimal(10,2)) as m4,
    cast(sum(case month(vstdate) when 5 then cprodu  end)/3 as decimal(10,2)) as m5,
    cast(sum(case month(vstdate) when 6 then cprodu  end)/3 as decimal(10,2)) as m6,
    cast(sum(case month(vstdate) when 7 then cprodu  end)/3 as decimal(10,2)) as m7,
    cast(sum(case month(vstdate) when 8 then cprodu  end)/3 as decimal(10,2)) as m8,
    cast(sum(case month(vstdate) when 9 then cprodu  end)/3 as decimal(10,2)) as m9,
    cast(sum(case month(vstdate) when 10 then cprodu  end)/3 as decimal(10,2)) as m10,
    cast(sum(case month(vstdate) when 11 then cprodu  end)/3 as decimal(10,2)) as m11,
    cast(sum(case month(vstdate) when 12 then cprodu  end)/3 as decimal(10,2)) as m12 
    from productivity_lr where year(vstdate) = '${year}'`
    let data:any = await db.raw(sql);
    return data[0];
  }
//รายงานอัตรากำลังเฉลี่ยรายเดือน/รายปี/หอผู้ป่วย
async select_pd_month(db: knex,year: any){
  let sql: any = 
  `select d.name,
  round(sum(case month(vstdate) when 1 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m1,
  round(sum(case month(vstdate) when 2 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m2,
  round(sum(case month(vstdate) when 3 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m3,
  round(sum(case month(vstdate) when 4 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m4,
  round(sum(case month(vstdate) when 5 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m5,
  round(sum(case month(vstdate) when 6 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m6,
  round(sum(case month(vstdate) when 7 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m7,
  round(sum(case month(vstdate) when 8 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m8,
  round(sum(case month(vstdate) when 9 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m9,
  round(sum(case month(vstdate) when 10 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m10,
  round(sum(case month(vstdate) when 11 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m11,
  round(sum(case month(vstdate) when 12 then cprodu end)/ day(LAST_DAY(vstdate))/3,2) as m12,
  round(round(sum(ptday)/12,2)/12,2) as ctotal 
  from productivity_lr  
  INNER JOIN l_dprtm as d on productivity_lr.codewd = d.code 
  where year(vstdate) = '${year}' group by d.name` 
  let data:any = await db.raw(sql);
  return data[0];

}

async select_pd_pttype(db: knex,datestart: any,dateend: any){
  let sql:any = 
  `select d.name,
  sum(pt1) as pt1,
  sum(pt2) as pt2, 
  sum(pt3) as pt3, 
  sum(pt4) as pt4, 
  sum(pt5) as pt5 
   from productivity_lr  as p
  INNER JOIN l_dprtm as d on p.codewd = d.code
   where p.vstdate between '${datestart}' and '${dateend}' group by d.name`
   let data: any = await db.raw(sql);
   return data[0];
}

}