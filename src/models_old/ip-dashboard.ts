import Knex, * as knex from 'knex';

export class IpDashboardModel {

  constructor() { }

  select_idpm(db: knex) {
    return db('idpm');
  }

  select_yearbudget(db:knex){
    return db('yearbudget')
  }

  async selete_raw_code(db: knex,code:any){
    let sql:any = `SELECT u.id,u.code,u.fullname,d.code as codedpm,d.name as namedpm FROM users u 
    INNER JOIN l_dprtm d on d.code = u.code 
    WHERE u.code = '${code}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }


async service_ip_all(db: Knex, date_start:any,date_end: any,ward: any){
 
    if (ward=='' || ward == 'null'){     
      let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
      where  date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
      where  i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }

  }

  async service_ip_null(db: Knex, date_start:any,date_end: any,ward: any){
    if (ward=='' || ward == 'null'){     
      let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
      where  i.dchtype = 0 and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
      where  i.dchtype = 0 and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }
  
  }
  
async service_ip_appro(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 1 and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 1 and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }


}

async service_ip_advice(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 2 and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 2 and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }


}

async service_ip_escape(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 3 and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 3 and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

async service_ip_refer(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 4 and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 4 and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}


async service_ip_other(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 5 and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = 5 and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }
}

async service_ip_nulltype(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = '' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype = '' and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }
}
async service_ip_dead(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype in(8,9) and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `select count(distinct i.hn) as countHN,count(distinct i.vn) as countVN from ipt as i
    where  i.dchtype in(8,9) and i.ward ='${ward}' and date(i.dchdate) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

async getIpVisitByWard(db: Knex, yearbudget: any,ward: any) {
  if (ward=='' || ward == 'null'){
    let data = await db('ipt as i')
    .innerJoin('idpm as c', 'c.idpm', 'i.ward')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .count('i.an as value')
    .select('c.nameidpm as name')
    .groupBy('i.ward');
    return data;
  }else{
    let data = await db('ipt as i')
    .innerJoin('idpm as c', 'c.idpm', 'i.ward')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('i.ward',ward)
    .count('i.an as value')
    .select('c.nameidpm as name')
    .groupBy('i.ward');
    return data;
  }

}

async getIpVisitByAge(db: Knex, yearbudget: any,ward: any) {
  if (ward =='' || ward == 'null'){
      let data = await db('ipt as i')
      .innerJoin('pt', 'pt.hn', 'i.hn')
      .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
      .count('i.an as value')
      .select(db.raw('count(case pt.male when "1" then i.an end) as male'))
      .select(db.raw('count(case pt.male when "2" then i.an end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,i.dchdate) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
    return data;
  }else{
      let data = await db('ipt as i')
      .innerJoin('pt', 'pt.hn', 'i.hn')
      .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
      .andWhere('i.ward',ward)
      .count('i.an as value')
      .select(db.raw('count(case pt.male when "1" then i.an end) as male'))
      .select(db.raw('count(case pt.male when "2" then i.an end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,i.dchdate) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,i.dchdate) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
      return data;
  }

}

async getIpVisitByGender(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward =='null'){
    let data = await db('ipt as i')
    .innerJoin('pt', 'pt.hn', 'i.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .count('i.an as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }else{
      let data = await db('ipt as i')
      .innerJoin('pt', 'pt.hn', 'i.hn')
      .innerJoin('male', 'pt.male', 'male.male')
      .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
      .andWhere('i.ward',ward)
      .count('i.an as value')
      .select('male.namemale as name')
      .groupBy('pt.male');
      return data; 
  }
}

async getIpVisitByMonth(db: Knex, yearbudget: any,ward: any) {
  if(ward =='' || ward =='null'){
    let data = await db('ipt as i')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .count('i.an as value')
    .select(db.raw('month(i.dchdate) as name'))
    .orderBy('i.dchdate')
    .groupBy('name');
    return data;
  }else{
    let data = await db('ipt as i')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('i.ward',ward)
    .count('i.an as value')
    .select(db.raw('month(i.dchdate) as name'))
    .orderBy('i.dchdate')
    .groupBy('name');
    return data;
  }

}

async getIpVisitByPttype(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward=='null'){
    let data = await db('ipt as i')
    .innerJoin('pttype as p', 'p.pttype', 'i.pttype')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .count('i.an as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }else{
    let data = await db('ipt as i')
    .innerJoin('pttype as p', 'p.pttype', 'i.pttype')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('i.ward',ward)
    .count('i.an as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }
}

async getIpVisit_IncothByMonth(db: Knex, yearbudget: any,ward: any) {
  if(ward =='' || ward=='null'){
    let data = await db('ipt as i')
    .innerJoin('pttype as p','p.pttype','i.pttype')
    .innerJoin('incoth as n','n.vn','i.vn')
    .innerJoin('idpm as c','c.idpm','i.ward')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1)= ? ', [yearbudget]))           
    .select(db.raw('month(i.dchdate) as name'))
    .countDistinct('i.vn as value')
    .sum('n.rcptamt as total')
    .groupBy('name')
    .orderBy('i.dchdate')
    return data;
  }else{
    let data = await db('ipt as i')
    .innerJoin('pttype as p','p.pttype','i.pttype')
    .innerJoin('incoth as n','n.vn','i.vn')
    .innerJoin('idpm as c','c.idpm','i.ward')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1)= ? ', [yearbudget]))  
    .andWhere('i.ward',ward)         
    .select(db.raw('month(i.dchdate) as name'))
    .countDistinct('i.vn as value')
    .sum('n.rcptamt as total')
    .groupBy('name')
    .orderBy('i.dchdate')
    return data;
  }
}
//สรุปค่ารักษา + total visit
async getIpVisit_IncothByMonthPttype_Inscl(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward=='null'){
    let data = await db('ipt as i')
    .innerJoin('pttype as p','p.pttype','i.pttype')
    .innerJoin('incoth as n','n.vn','i.vn')
    .innerJoin('idpm as c','c.idpm','i.ward')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1)= ? ', [yearbudget]))            
    .select('p.inscl as name')
    .countDistinct('i.vn as value')
    .sum('n.rcptamt as total')
    .groupBy('p.inscl')
     return data;
  }else {
    let data = await db('ipt as i')
    .innerJoin('pttype as p','p.pttype','i.pttype')
    .innerJoin('incoth as n','n.vn','i.vn')
    .innerJoin('idpm as c','c.idpm','i.ward')
    .where(db.raw('if(month(i.dchdate) < 10,year(i.dchdate),year(i.dchdate)+1)= ? ', [yearbudget])) 
    .andWhere('i.ward',ward)           
    .select('p.inscl as name')
    .countDistinct('i.vn as value')
    .sum('n.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }

}

}