import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StmIpOfcModel } from '../models/hicm-ip-ofc'

import { log } from 'console'

const fromImport = new StmIpOfcModel()

export default async function test(fastify: FastifyInstance) {

  const db = fastify.hicm;
  
  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'STM IP LGO Model' })
  })

  fastify.post('/select_debt_account', async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const rs: any = await fromImport.select_debt_account(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.post('/select_debt_account_hi', async (request: FastifyRequest, reply: FastifyReply) => {
    const dbhi = fastify.hi;
    try {
      const rs: any = await fromImport.select_debt_account_hi(dbhi)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/stm_countan', async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const info:any = req.body;
    console.log(info);
    
    try {
        let rs:any = await fromImport.stm_countan(db,info.accType,info.startDate,info.endDate); 
  
        if (!rs.length) {
            let datas = {
                "ok": false,
                "text": "การบันทึกข้อมูลผิดพลาด",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204,results: datas })
          } else {
            let datas = {
                "ok": true,
                "text": "การบันทึกข้อมูลสำเร็จ",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204, results: datas})
          }
    } catch (error:any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });     
    } 

});

  fastify.post('/stm_ip_ofc', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const info:any = req.body;
    console.log(db);
    try {
        let rs:any = await fromImport.select_stm_ip_ofc(db,info.repno); 
        if (!rs.length) {
            let datas = {
                "ok": false,
                "text": "การอ่านข้อมูลผิดพลาด",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204,results: datas })
          } else {
            let datas = {
                "ok": true,
                "text": "การอ่านข้อมูลสำเร็จ",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204, results: datas})
          }
    } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });        
    } 
}); 

fastify.post('/stm_ip_ofc_stmno', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;
  const info:any = req.body;
  console.log(db);
  try {
      let rs:any = await fromImport.select_stm_ip_ofc_stmno(db,info.stm_no); 
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การอ่านข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การอ่านข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });        
  } 
}); 

fastify.post('/ipofcnull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipofcnull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ results: datas })
              } else {

                for (const i of rs) {

                    if (i.dchdate) {
                    const date = new Date(i.dchdate);
                    const year = date.getUTCFullYear();
                    const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                    const day = String(date.getUTCDate()).padStart(2, '0');
                    const formattedDate = `${year}-${month}-${day}`;
                    i.dchdate=formattedDate;
                    }         
                    if (i.admitdate) {
                        const date = new Date(i.admitdate);
                        const year = date.getUTCFullYear();
                        const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                        const day = String(date.getUTCDate()).padStart(2, '0');
                        const formattedDate = `${year}-${month}-${day}`;
                        i.admitdate=formattedDate;
                     }                 
                }

                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });    
        } 
    
    });

    fastify.post('/ipofcaccnotnull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipofcaccnotnull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204, results: datas})
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });

    fastify.post('/ipofcnotnull_stm', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipofcnotnull_stm(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });
    fastify.post('/ipofcaccnotnull_stm', async (request: FastifyRequest, reply: FastifyReply) => {
      const req:any = request;
      const info:any = req.body;
      console.log(info);
      
      try {
          let rs:any = await fromImport.ipofcaccnotnull_stm(db,info.accType,info.startDate,info.endDate); 
    
          if (!rs.length) {
              let datas = {
                  "ok": false,
                  "text": "การบันทึกข้อมูลผิดพลาด",
                  "rows": rs
              };
              reply.code(200).send({ statusCode: 204,results: datas })
            } else {
              let datas = {
                  "ok": true,
                  "text": "การบันทึกข้อมูลสำเร็จ",
                  "rows": rs
              };
              reply.code(200).send({ statusCode: 204,results: datas })
            }
      } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });     
      } 
  
  });
    fastify.post('/ipofcaccnull', async (request: FastifyRequest, reply: FastifyReply) => {
      const req:any = request;
      const info:any = req.body;
      console.log(info);
      
      try {
          let rs:any = await fromImport.ipofcaccnull(db,info.accType,info.startDate,info.endDate); 
    
          if (!rs.length) {
              let datas = {
                  "ok": false,
                  "text": "การบันทึกข้อมูลผิดพลาด",
                  "rows": rs
              };
              reply.code(200).send({ statusCode: 204,results: datas })
            } else {
              let datas = {
                  "ok": true,
                  "text": "การบันทึกข้อมูลสำเร็จ",
                  "rows": rs
              };
              reply.code(200).send({ statusCode: 204,results: datas })
            }
      } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });     
      } 
  
  });

    fastify.post('/ipofcnotnull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipofcnotnull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });       
        } 
    
    });
      
    fastify.post('/ipofcaccbydate', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipofcaccbydate(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
             
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
              
                for (const i of rs) {

                    if (i.dchdate) {
                    const date = new Date(i.dchdate);
                    const year = date.getUTCFullYear();
                    const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                    const day = String(date.getUTCDate()).padStart(2, '0');
                    const formattedDate = `${year}-${month}-${day}`;
                    i.dchdate=formattedDate;
                    }                    
                }
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });

  
    fastify.post('/stm_ip_ofc_sum', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.stm_ip_ofc_sum(db,info.repno); 
    
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })

        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });

    fastify.post('/stm_ip_ofc_sum_stmno', async (request: FastifyRequest, reply: FastifyReply) => {
      const req:any = request;
      const info:any = req.body;
      console.log(info);
      
      try {
          let rs:any = await fromImport.stm_ip_ofc_sum_stmno(db,info.stm_no); 
  
              let datas = {
                  "ok": true,
                  "text": "การบันทึกข้อมูลสำเร็จ",
                  "rows": rs
              };
              reply.code(200).send({ statusCode: 204,results: datas })

      } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });     
      } 
  
  });

    fastify.post('/stm_ip_ofc_detail', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);   
        try {
            let rs:any = await fromImport.stm_ip_ofc_detail(db,info.repno); 
    
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })

        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });
    fastify.post('/toperrorcode', async (request: FastifyRequest, reply: FastifyReply) => {
      const req:any = request;
      const info:any = req.body;
      console.log(info);
      
      try {
          let rs:any = await fromImport.toperrorcode(db,info.accType,info.startDate,info.endDate); 
    
          if (!rs.length) {
              let datas = {
                  "ok": false,
                  "text": "การบันทึกข้อมูลผิดพลาด",
                  "rows": rs
              };
              reply.code(200).send({ results: datas })
            } else {

              for (const i of rs) {

                  if (i.dchdate) {
                  const date = new Date(i.dchdate);
                  const year = date.getUTCFullYear();
                  const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                  const day = String(date.getUTCDate()).padStart(2, '0');
                  const formattedDate = `${year}-${month}-${day}`;
                  i.dchdate=formattedDate;
                  }         
                  if (i.admitdate) {
                      const date = new Date(i.admitdate);
                      const year = date.getUTCFullYear();
                      const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                      const day = String(date.getUTCDate()).padStart(2, '0');
                      const formattedDate = `${year}-${month}-${day}`;
                      i.admitdate=formattedDate;
                   }                 
              }

              let datas = {
                  "ok": true,
                  "text": "การบันทึกข้อมูลสำเร็จ",
                  "rows": rs
              };
              reply.code(200).send({ results: datas })
            }
      } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });    
      } 
  
  });       
}
