import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { IPReferModel } from '../models/ip-refer'
import { log } from 'console'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new IPReferModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'OPRefer Model' })
  })

  fastify.get('/select_cln', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    try {
      const rs: any = await fromImport.select_cln(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.get('/select_yearbudget', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hicm;
    try {
      const rs: any = await fromImport.select_yearbudget(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


fastify.post('/getIPReferoutsummary', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req :any = request.body;
  const yearbudget :any = req.yearbudget;
  const ward = req.ward;
  let info :any = {};
  try {
      const clinic = await fromImport.getReferoutByWard(db, yearbudget,ward);
      if(clinic.length > 0){

      }
      const gender = await fromImport.getReferoutByGender(db, yearbudget,ward);
      const age = await fromImport.getReferoutByAge(db, yearbudget,ward);
      const month = await fromImport.getReferoutByMonth(db, yearbudget,ward);
      const pttype = await fromImport.getReferoutByPttype(db, yearbudget,ward);
      const sincothmm = await fromImport.getReferout_IncothByMonth(db, yearbudget,ward);
      const sincothinscl = await fromImport.getReferout_IncothByMonthPttype_Inscl(db,yearbudget,ward);
      const maxopvisit_m = await fromImport.getMaxReferoutbyMonth(db,yearbudget,ward);
      info = {
          ok: true,
          text: "รายงานข้อมูลผู้ป่วยนอก",
          clinic: clinic,
          gender: gender,
          age: age,
          month: month,
          pttype: pttype,
          sincothmm:sincothmm,
          sincothinscl:sincothinscl,
          maxopvisit_m:maxopvisit_m
      }
      reply.code(200).send(info);

  } catch (error :any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });
  }
});

fastify.post('/getOPReferinsummary', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req :any = request.body;
  const yearbudget :any = req.yearbudget;
  const ward = req.ward;
  let info :any = {};
  try {
      const clinic = await fromImport.getReferinByWard(db, yearbudget,ward);
      if(clinic.length > 0){

      }
      const gender = await fromImport.getReferinByGender(db, yearbudget,ward);
      const age = await fromImport.getReferinByAge(db, yearbudget,ward);
      const month = await fromImport.getReferinByMonth(db, yearbudget,ward);
      const pttype = await fromImport.getReferinByPttype(db, yearbudget,ward);
      const sincothmm = await fromImport.getReferin_IncothByMonth(db, yearbudget,ward);
      const sincothinscl = await fromImport.getReferin_IncothByMonthPttype_Inscl(db,yearbudget,ward);
      const maxopvisit_m = await fromImport.getMaxReferinbyMonth(db,yearbudget,ward);
      info = {
          ok: true,
          text: "รายงานข้อมูลผู้ป่วยนอก",
          clinic: clinic,
          gender: gender,
          age: age,
          month: month,
          pttype: pttype,
          sincothmm:sincothmm,
          sincothinscl:sincothinscl,
          maxopvisit_m:maxopvisit_m
      }
      reply.code(200).send(info);

  } catch (error :any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });
  }
});

fastify.post('/getnameward',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let codecln = req.ward;
  const ward = codecln;

  try {
    const rs: any = await fromImport.getNameWard(db,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
});


fastify.post('/getDx20Referout', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getDx20Referout(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getDx20Referin', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getDx20Referin(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getCLN20Referout', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getCLN20Referout(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getCLN20Referin', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getCLN20Referin(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getRfrcs20Referout', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getRfrcs20Referout(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getRfrcs20Referin', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getRfrcs20Referin(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getHcode20Referout', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getHcode20Referout(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});

fastify.post('/getHcode20Referin', async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codecln;
  console.log(end_date);
  
  try {
      let rs:any = await fromImport.getHcode20Referin(db, start_date, end_date,ward); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});
}
