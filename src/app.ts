import * as fastify from 'fastify'
import WebSocket from 'ws'
import { join } from 'path'

const multer = require('fastify-multer')
//อ่านไฟล์ config ในส่วนติดต่อฐานข้อมูล ถ้ามีการเปลี่ยนชื่อต้องเปลี่ยนส่วนนี้ตามด้วย
require('dotenv').config({ path: join(__dirname, '../config') })

const app: fastify.FastifyInstance = fastify.fastify({
  logger: { level: 'info' }
})


app.register(multer.contentParser)

app.register(require('fastify-formbody'))
app.register(require('fastify-cors'), {})
// First connection
app.register(require('./plugins/db'), {
  connectionName: 'mysql',
  options: {
    client: 'mysql',
    connection: {
      host: process.env.DB_HOST || 'localhost',
      user: process.env.DB_USER || 'root',
      port: Number(process.env.DB_PORT) || 3306,
      password: process.env.DB_PASSWORD || '',
      database: process.env.DB_NAME || 'test',
    },
    pool: {
      min: 0,
      max: 100
    },
    debug: true,
  }
})
// Second connection
app.register(require('./plugins/db'), {
  connectionName: 'mysql2',
  options: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST || 'localhost',
      user: process.env.DB_USER || 'root',
      port: Number(process.env.DB_PORT) || 3306,
      password: process.env.DB_PASSWORD || '',
      database: process.env.DB_NAME || 'test',
    },
    pool: {
      min: 0,
      max: 100
    },
    debug: true,
  }
})

// HI connection
app.register(require('./plugins/db'), {
  connectionName: 'hi',
  options: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST_HI || 'localhost',
      user: process.env.DB_USER_HI || 'root',
      port: Number(process.env.DB_PORT_HI) || 3306,
      password: process.env.DB_PASSWORD_HI || '',
      database: process.env.DB_NAME_HI || 'test',
    },
    pool: {
      min: 0,
      max: 100
    },
    debug: true,
  }
})

// HICM connection
app.register(require('./plugins/db'), {
  connectionName: 'hicm',
  options: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST_HICM || 'localhost',
      user: process.env.DB_USER_HICM || 'root',
      port: Number(process.env.DB_PORT_HICM) || 3306,
      password: process.env.DB_PASSWORD_HICM || '',
      database: process.env.DB_NAME_HICM || 'test',
    },
    pool: {
      min: 0,
      max: 100
    },
    debug: true,
  }
})

// pg connection
app.register(require('./plugins/db'), {
  connectionName: 'pg',
  options: {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST || 'localhost',
      user: process.env.DB_USER || 'root',
      port: Number(process.env.DB_PORT) || 3306,
      password: process.env.DB_PASSWORD || '',
      database: process.env.DB_NAME || 'test',
    },
    pool: {
      min: 0,
      max: 100
    },
    debug: true,
  }
})

app.register(require('./plugins/jwt'), {
  secret: process.env.SECRET_KEY || '@1234567890@'
})

app.register(require('./plugins/ws'), {
  path: '/ws',
  maxPayload: 1048576,
  verifyClient: function (info: any, next: any) {
    if (info.req.headers['x-fastify-header'] !== 'fastify') {
      return next(false)
    }
    next(true)
  }
})

// Axios
app.register(require('fastify-axios'), {
  clients: {
    v1: {
      baseURL: 'https://apingweb.com/api/rest',
    },
    v2: {
      baseURL: 'https://randomuser.me/api'
    }
  }
})

// QR Code
app.register(require('@chonla/fastify-qrcode'))

app.register(require('./routes/index'), { prefix: '/' });
app.register(require('./routes/demo'), { prefix: '/demo' });
app.register(require('./routes/test'), { prefix: '/test' });
app.register(require('./routes/login'), { prefix: '/login' });
app.register(require('./routes/users'), { prefix: '/users' });
app.register(require('./routes/productivity'), { prefix: '/productivity' });
app.register(require('./routes/productivitylr'), { prefix: '/productivitylr' });
app.register(require('./routes/productivityer'), { prefix: '/productivityer' });
//OP DashBoard
app.register(require('./routes/dashboard'),{ prefix:'/dashboard'});
//IP Dashboard
app.register(require('./routes/ip-dashboard'),{ prefix:'/ip-dashboard'});
//statement op
app.register(require('./routes/hicm-ip-sss'),{ prefix:'/hicm-ip-sss'});
app.register(require('./routes/hicm-ip-ofc'),{ prefix:'/hicm-ip-ofc'});
app.register(require('./routes/hicm-ip-stp'),{ prefix:'/hicm-ip-stp'});
app.register(require('./routes/hicm-ip-ucs'),{ prefix:'/hicm-ip-ucs'});
app.register(require('./routes/hicm-ip-bkk'),{ prefix:'/hicm-ip-bkk'});
//statement op
app.register(require('./routes/hicm-op-ofc'),{ prefix:'/hicm-op-ofc'});
app.register(require('./routes/hicm-op-stp'),{ prefix:'/hicm-op-stp'});
app.register(require('./routes/hicm-op-ucs'),{ prefix:'/hicm-op-ucs'});
app.register(require('./routes/hicm-op-bkk'),{ prefix:'/hicm-op-bkk'});
// aips
app.register(require('./routes/aipn'), { prefix: '/aipn' });
// ssop
app.register(require('./routes/ssop'), { prefix: '/ssop' });
// op refer
app.register(require('./routes/op-refer'), { prefix: '/op-refer' });
app.register(require('./routes/ip-refer'), { prefix: '/ip-refer' });
// 16f
app.register(require('./routes/16f'), { prefix: '/16f' });

// plugins
app.register(require('./routes/upload'), { prefix: '/upload' });
app.register(require('./routes/schema'), { prefix: '/schema' });


const port = process.env.PORT || 3000
const address = process.env.HOST || '0.0.0.0'

const start = async () => {
  try {
    await app.listen(port, address)
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}

start()

export default app;