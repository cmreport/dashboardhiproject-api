import { log } from 'console';
import Knex, * as knex from 'knex';

export class StmIpSssModel {

  constructor() { }

  select(db: knex) {
    return db('stm_ipop_sss');
  }

  async select_debt_account(db: knex) {
    let sql:any  =  `SELECT * from debt_account as s
        WHERE s.debt_type = 'IPD' `;
     console.log('sql:',sql);   
     let data: any =  await db.raw(sql)
    return data[0];
  }

  async select_stm_ip_sss(db: Knex,repno: number) {
    let sql:any  =  `SELECT s.*,(s.charge - s.total_summary) as diff from stm_ipop_sss as s
        WHERE s.repno = ? `;
     console.log('sql:',sql);
    let data: any = await db.raw(sql, [repno])
    
    return data[0];
         
  }   
  async ipsssnull(db: Knex, accType: any, startDate: any, endDate: any) {
    let sql: any = `SELECT * from (
        SELECT
            d.hn,
            d.an,
            d.cid,
            d.fullname,
            (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
            DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
            DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
          CASE
                
                WHEN dchdate = admitdate THEN
                1 ELSE DATEDIFF( dchdate, admitdate ) 
            END AS l_stay,
            d.charge AS charge,
            d.paid AS paid,
            d.debt AS debt,
            d.debt AS debt2,
            r.repno AS repno,
            r.errorcode AS error_code,
            v.code_name AS error_name,
            v.remarkdata AS remark_data,
            r.total_paid AS total_paid,
            s.adjrw,
          CASE
                
                WHEN r.total_paid IS NOT NULL THEN
                ( d.debt - r.total_paid ) ELSE NULL 
            END AS rep_diff,
          CASE
                
                WHEN ( d.debt - r.total_paid ) <= 0 THEN
                d.debt ELSE ( r.total_paid ) 
            END AS rep_diff2,
          CASE
                
                WHEN ( d.debt - r.total_paid ) <= 0 THEN
                ( r.total_paid - d.debt ) ELSE 0 
            END AS rest_debt,s.total_summary 
          FROM
            debit_ip AS d
            LEFT JOIN stm_ipop_sss AS s ON d.an = s.an
            LEFT JOIN rep_ipop_sss AS r ON d.an = r.an
            LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
          WHERE
            d.acc_code = ? 
            AND d.dchdate BETWEEN ?
            AND ?
          GROUP BY d.an 
          ORDER BY
            d.dchdate ) as x where repno is null or error_code != '-' `;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
}

async ipsssnotnull(db: Knex, accType: any, startDate: any, endDate: any) {
    let sql: any = `SELECT *   from (
        SELECT	d.hn,
        d.an,
        d.cid,
        d.fullname,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
        DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
      CASE
            
            WHEN dchdate = admitdate THEN
            1 ELSE DATEDIFF( dchdate, admitdate ) 
        END AS l_stay,
        d.charge AS charge,
        d.paid AS paid,
        d.debt AS debt,
        d.debt AS debt2,
        r.repno AS repno,
        r.errorcode AS error_code,
        v.code_name AS error_name,
        v.remarkdata AS remark_data,
        r.total_paid AS total_paid,
      s.adjrw,
      CASE
            
            WHEN r.total_paid IS NOT NULL THEN
            ( d.debt - r.total_paid ) ELSE NULL 
        END AS rep_diff,
      CASE
            
            WHEN ( d.debt - r.total_paid ) <= 0 THEN
            d.debt ELSE ( r.total_paid ) 
        END AS rep_diff2,
      CASE
            
            WHEN ( d.debt - r.total_paid ) <= 0 THEN
            ( r.total_paid - d.debt ) ELSE 0 
        END AS rest_debt,s.total_summary 
      FROM
        debit_ip AS d
        LEFT JOIN stm_ipop_sss AS s ON d.an = s.an
        LEFT JOIN rep_ippo_sss AS r ON d.an = r.an
        LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
      WHERE
        d.acc_code = ?
        AND d.dchdate BETWEEN ?
        AND ?  
      Group by d.an
      ORDER BY
        d.dchdate ) as x where repno is not null and error_code = '-'`;
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0]; 
}

async ipsssaccnull(db: Knex, accType: any, startDate: any, endDate: any) {
    let sql: any = `SELECT COUNT( an) AS count_an , 
    SUM(debt ) AS  sum_debt   from (
SELECT
	d.hn,
	d.an,
	d.cid,
	d.fullname,
	(SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
	DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
	DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
  CASE
		
		WHEN dchdate = admitdate THEN
		1 ELSE DATEDIFF( dchdate, admitdate ) 
	END AS l_stay,
	d.charge AS charge,
	d.paid AS paid,
	d.debt AS debt,
	d.debt AS debt2,
	r.repno AS repno,
	r.errorcode AS error_code,
	v.code_name AS error_name,
	v.remarkdata AS remark_data,
	r.total_paid AS total_paid,
    s.adjrw,
  CASE
		
		WHEN r.total_paid IS NOT NULL THEN
		( d.debt - r.total_paid ) ELSE NULL 
	END AS rep_diff,
  CASE
		
		WHEN ( d.debt - r.total_paid ) <= 0 THEN
		d.debt ELSE ( r.total_paid ) 
	END AS rep_diff2,
  CASE
		
		WHEN ( d.debt - r.total_paid ) <= 0 THEN
		( r.total_paid - d.debt ) ELSE 0 
	END AS rest_debt,s.total_summary 
  FROM
	debit_ip AS d
	LEFT JOIN stm_ipop_sss AS s ON d.an = s.an
	LEFT JOIN rep_ipop_sss AS r ON d.an = r.an
	LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
  WHERE
	d.acc_code = ? 
	AND d.dchdate BETWEEN ? 
	AND ?
  GROUP BY d.an 
  ORDER BY
	d.dchdate ) as x where repno is null or error_code != '-' `;
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];
}

async ipsssaccnotnull(db: Knex, accType: any, startDate: any, endDate: any) {
    let sql: any = `SELECT COUNT( an) AS all_notnullcase , 
    SUM(debt ) AS  debit_notnull, 
    SUM(total_summary ) AS recieve, 
    SUM(debt-total_summary) AS sum_diff , 
    SUM(CASE WHEN (debt-total_summary) > 0 THEN (debt-total_summary) ELSE 0 END) AS diffgain ,  
    SUM(CASE WHEN (debt-total_summary) < 0 THEN (debt-total_summary) ELSE 0 END) AS diffloss   from (
    SELECT	d.hn,
	d.an,
	d.cid,
	d.fullname,
	(SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
	DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
	DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
  CASE
		
		WHEN dchdate = admitdate THEN
		1 ELSE DATEDIFF( dchdate, admitdate ) 
	END AS l_stay,
	d.charge AS charge,
	d.paid AS paid,
	d.debt AS debt,
	d.debt AS debt2,
	r.repno AS repno,
	r.errorcode AS error_code,
	v.code_name AS error_name,
	v.remarkdata AS remark_data,
	r.total_paid AS total_paid,
    s.adjrw,
  CASE
		
		WHEN r.total_paid IS NOT NULL THEN
		( d.debt - r.total_paid ) ELSE NULL 
	END AS rep_diff,
  CASE
		
		WHEN ( d.debt - r.total_paid ) <= 0 THEN
		d.debt ELSE ( r.total_paid ) 
	END AS rep_diff2,
  CASE
		
		WHEN ( d.debt - r.total_paid ) <= 0 THEN
		( r.total_paid - d.debt ) ELSE 0 
	END AS rest_debt,s.total_summary 
  FROM
	debit_ip AS d
	LEFT JOIN stm_ipop_sss AS s ON d.an = s.an
	LEFT JOIN stm_ipop_sss AS r ON d.an = r.an
	LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
  WHERE
	d.acc_code = ? 
	AND d.dchdate BETWEEN ? 
	    AND ? 
  Group by d.an
  ORDER BY
	d.dchdate ) as x where repno is not null and error_code = '-'`;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
}

async ipsssaccbydate(db: Knex, accType: any, startDate: any, endDate: any) {
    let sql: any = `SELECT 
    d.dchdate
    , COUNT(d.an)  AS allcase 
    , SUM(d.debt) AS  debit 
    , SUM(CASE WHEN s.repno IS NULL THEN 1 ELSE 0 END)  AS nullcase,
    SUM(CASE WHEN s.repno IS  NULL THEN d.debt ELSE 0 END)  AS nulldebit ,
    SUM(CASE WHEN s.repno IS NOT NULL THEN 1 ELSE 0 END)  AS notnullcase
    ,SUM(CASE WHEN s.repno IS NOT NULL THEN d.debt ELSE 0 END)  AS notnulldebit,
    SUM(s.total_summary) AS recieve 
    ,SUM(CASE WHEN s.repno IS NOT NULL THEN d.debt ELSE 0 END)-SUM(s.total_summary) AS diff 
     FROM debit_ip AS d LEFT OUTER JOIN stm_ipop_sss AS s ON d.an = s.an  
    WHERE d.acc_code = ?
    AND dchdate BETWEEN ? AND ?  GROUP BY d.dchdate`;
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];
}

async stm_ip_sss_sum(db: Knex, repno: any) {
    let sql: any = `SELECT
    stm.repno,
    COUNT(stm.an) AS "all_case",
    SUM(CAST(stm.charge AS DECIMAL)) AS "debt",
    SUM(CAST(stm.total_summary AS DECIMAL)) AS "receive",
    SUM(stm.diff) AS "sum_diff",
    SUM(CASE WHEN stm.diff > 0 THEN stm.diff ELSE 0 END) AS diffgain,
    SUM(CASE WHEN stm.diff < 0 THEN stm.diff ELSE 0 END) AS diffloss
FROM
    (
    SELECT
        sd.repno,
        sd.an,
        CAST(sd.charge AS DECIMAL) AS charge,
        CAST(sd.total_summary AS DECIMAL) AS total_summary,
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_ipop_sss sd
    WHERE
        sd.repno = ?
    ORDER BY
        sd.date_dch
    ) stm
GROUP BY
    stm.repno`;
    let data: any = await db.raw(sql, [repno])
    return data[0];
}

async stm_ip_sss_detail(db: Knex, repno: any) {
    let sql: any = `SELECT
    stm.*
FROM
    (
    SELECT
         sd.*,       
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_ipop_sss sd
    WHERE
        sd.repno = ?
     ORDER BY
        sd.date_dch
    ) stm`;
let data: any = await db.raw(sql, [repno])
    return data[0];
}
async stm_countan(db:Knex,accType:any,startDate:any,endDate:any){
    let sql:any =` SELECT 
    COUNT( DISTINCT d.an) AS all_ip 
    FROM debit_ip AS d 
    WHERE d.acc_code = ?
	AND d.dchdate BETWEEN ? 
	    AND ? `
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];  
}
}