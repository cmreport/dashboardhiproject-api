import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { AipnModel } from '../models/aipn'
import { log } from 'console'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new AipnModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'AIPN Model' })
  })

  fastify.get('/select_idpm', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    try {
      const rs: any = await fromImport.select_idpm(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.get('/select_yearbudget', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hicm;
    try {
      const rs: any = await fromImport.select_yearbudget(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/aipn_all_statusflg',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hicm;
    const req:any = request;
    const info:any = req.body;    
    try {
      const rs: any = await fromImport.aipn_all_statusflg(db, info.ward,info.startDate,info.endDate)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
})

fastify.post('/aipn_statusflgN',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {  
  const db: any = fastify.hicm;
  const req:any = request;
  const info:any = req.body;    
  try {
    let rs:any = await fromImport.aipn_statusflgN(db,info.ward,info.startDate,info.endDate); 

    if (!rs.length) {
        let datas = {
            "ok": false,
            "text": "การบันทึกข้อมูลผิดพลาด",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      } else {

        for (const i of rs) {

            if (i.dchdate) {
            const date = new Date(i.dchdate);
            const year = date.getUTCFullYear();
            const month = String(date.getUTCMonth() + 1).padStart(2, '0');
            const day = String(date.getUTCDate()).padStart(2, '0');
            const formattedDate = `${year}-${month}-${day}`;
            i.dchdate=formattedDate;
            }         
            if (i.dtadm) {
                const date = new Date(i.dtadm);
                const year = date.getUTCFullYear();
                const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                const day = String(date.getUTCDate()).padStart(2, '0');
                const formattedDate = `${year}-${month}-${day}`;
                i.dtadm=formattedDate;
             }                 
        }

        let datas = {
            "ok": true,
            "text": "การบันทึกข้อมูลสำเร็จ",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      }
} catch (error:any) {
  req.log.error(error);
  reply.code(500).send({
      ok: false,
      text: "การอ่านข้อมูลเกิดความผิดพลาด",
      error: error.message
  });    
} 
})

fastify.post('/aipn_statusflgA',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hicm;
  const req:any = request;
  const info:any = req.body;    
  try {
    let rs:any = await fromImport.aipn_statusflgA(db,info.ward,info.startDate,info.endDate); 

    if (!rs.length) {
        let datas = {
            "ok": false,
            "text": "การบันทึกข้อมูลผิดพลาด",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      } else {

        for (const i of rs) {

            if (i.dchdate) {
            const date = new Date(i.dchdate);
            const year = date.getUTCFullYear();
            const month = String(date.getUTCMonth() + 1).padStart(2, '0');
            const day = String(date.getUTCDate()).padStart(2, '0');
            const formattedDate = `${year}-${month}-${day}`;
            i.dchdate=formattedDate;
            }         
            if (i.dtadm) {
                const date = new Date(i.dtadm);
                const year = date.getUTCFullYear();
                const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                const day = String(date.getUTCDate()).padStart(2, '0');
                const formattedDate = `${year}-${month}-${day}`;
                i.dtadm=formattedDate;
             }                 
        }

        let datas = {
            "ok": true,
            "text": "การบันทึกข้อมูลสำเร็จ",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      }
} catch (error:any) {
  req.log.error(error);
  reply.code(500).send({
      ok: false,
      text: "การอ่านข้อมูลเกิดความผิดพลาด",
      error: error.message
  });    
} 


})

fastify.post('/aipn_statusflgC',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hicm;
  const req:any = request;
  const info:any = req.body;    
  try {
    let rs:any = await fromImport.aipn_statusflgC(db,info.ward,info.startDate,info.endDate); 

    if (!rs.length) {
        let datas = {
            "ok": false,
            "text": "การบันทึกข้อมูลผิดพลาด",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      } else {

        for (const i of rs) {

            if (i.dchdate) {
            const date = new Date(i.dchdate);
            const year = date.getUTCFullYear();
            const month = String(date.getUTCMonth() + 1).padStart(2, '0');
            const day = String(date.getUTCDate()).padStart(2, '0');
            const formattedDate = `${year}-${month}-${day}`;
            i.dchdate=formattedDate;
            }         
            if (i.dtadm) {
                const date = new Date(i.dtadm);
                const year = date.getUTCFullYear();
                const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                const day = String(date.getUTCDate()).padStart(2, '0');
                const formattedDate = `${year}-${month}-${day}`;
                i.dtadm=formattedDate;
             }                 
        }

        let datas = {
            "ok": true,
            "text": "การบันทึกข้อมูลสำเร็จ",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      }
} catch (error:any) {
  req.log.error(error);
  reply.code(500).send({
      ok: false,
      text: "การอ่านข้อมูลเกิดความผิดพลาด",
      error: error.message
  });    
} 
})

fastify.post('/aipn_statusflgD',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hicm;
  const req:any = request;
  const info:any = req.body;    
  try {
    let rs:any = await fromImport.aipn_statusflgD(db,info.ward,info.startDate,info.endDate); 

    if (!rs.length) {
        let datas = {
            "ok": false,
            "text": "การบันทึกข้อมูลผิดพลาด",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      } else {

        for (const i of rs) {

            if (i.dchdate) {
            const date = new Date(i.dchdate);
            const year = date.getUTCFullYear();
            const month = String(date.getUTCMonth() + 1).padStart(2, '0');
            const day = String(date.getUTCDate()).padStart(2, '0');
            const formattedDate = `${year}-${month}-${day}`;
            i.dchdate=formattedDate;
            }         
            if (i.dtadm) {
                const date = new Date(i.dtadm);
                const year = date.getUTCFullYear();
                const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                const day = String(date.getUTCDate()).padStart(2, '0');
                const formattedDate = `${year}-${month}-${day}`;
                i.dtadm=formattedDate;
             }                 
        }

        let datas = {
            "ok": true,
            "text": "การบันทึกข้อมูลสำเร็จ",
            "rows": rs
        };
        reply.code(200).send({ results: datas })
      }
} catch (error:any) {
  req.log.error(error);
  reply.code(500).send({
      ok: false,
      text: "การอ่านข้อมูลเกิดความผิดพลาด",
      error: error.message
  });    
} 
})

}
