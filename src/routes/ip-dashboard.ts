import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { IpDashboardModel } from '../models/ip-dashboard'
import { log } from 'console'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new IpDashboardModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'IP-Dashboard Model' })
  })

  fastify.get('/select_idpm', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    try {
      const rs: any = await fromImport.select_idpm(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.get('/select_yearbudget', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hicm;
    try {
      const rs: any = await fromImport.select_yearbudget(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/service_ip_all',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req: any = request.body;
    let yearbudget = req.yearbudget;
    let codeward = req.ward;

    if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
        yearbudget = new Date().getFullYear();
    }

    const start_date = (yearbudget - 1) + '-10-01';
    const end_date = yearbudget + '-09-30';
    const ward = codeward;
    console.log(end_date);
    
    try {
      const rs: any = await fromImport.service_ip_all(db, start_date, end_date,ward)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
})

  fastify.post('/service_ip_null',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req: any = request.body;
    let yearbudget = req.yearbudget;
    let codeward = req.ward;

    if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
        yearbudget = new Date().getFullYear();
    }

    const start_date = (yearbudget - 1) + '-10-01';
    const end_date = yearbudget + '-09-30';
    const ward = codeward;
    console.log(end_date);
    
    try {
      const rs: any = await fromImport.service_ip_null(db, start_date, end_date,ward)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
})
fastify.post('/service_ip_nulltype',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codeward = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codeward;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_ip_nulltype(db, start_date, end_date,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

  fastify.post('/service_ip_appro',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req: any = request.body;
    let yearbudget = req.yearbudget;
    let codeward = req.ward;

    if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
        yearbudget = new Date().getFullYear();
    }

    const start_date = (yearbudget - 1) + '-10-01';
    const end_date = yearbudget + '-09-30';
    const ward = codeward;
    console.log(end_date);
    
    try {
      const rs: any = await fromImport.service_ip_appro(db, start_date, end_date,ward)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
})
  
fastify.post('/service_ip_advice',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codeward = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codeward;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_ip_advice(db, start_date, end_date,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_ip_escape',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codeward = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codeward;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_ip_escape(db, start_date, end_date,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_ip_refer',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codeward = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codeward;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_ip_refer(db, start_date, end_date,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_ip_other',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codeward = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codeward;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_ip_other(db, start_date, end_date,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_ip_dead',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codeward = req.ward;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const ward = codeward;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_ip_dead(db, start_date, end_date,ward)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})


fastify.post('/getIPsummary', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req :any = request.body;
  const yearbudget :any = req.yearbudget;
  const cln = req.ward;
  let info :any = {};
  try {
      const clinic = await fromImport.getIpVisitByWard(db, yearbudget,cln);
      if(clinic.length > 0){

      }
      const gender = await fromImport.getIpVisitByGender(db, yearbudget,cln);
      const age = await fromImport.getIpVisitByAge(db, yearbudget,cln);
      const month = await fromImport.getIpVisitByMonth(db, yearbudget,cln);
      const pttype = await fromImport.getIpVisitByPttype(db, yearbudget,cln);
      const sincothmm = await fromImport.getIpVisit_IncothByMonth(db, yearbudget,cln);
      const sincothinscl = await fromImport.getIpVisit_IncothByMonthPttype_Inscl(db,yearbudget,cln);
      const maxopvisit_m = await fromImport.getMaxIpVisitbyMonth(db,yearbudget,cln);
      info = {
          ok: true,
          text: "รายงานข้อมูลผู้ป่วยนอก",
          clinic: clinic,
          gender: gender,
          age: age,
          month: month,
          pttype: pttype,
          sincothmm:sincothmm,
          sincothinscl:sincothinscl,
          maxopvisit_m:maxopvisit_m
      }
      reply.code(200).send(info);

  } catch (error :any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });
  }
});


}
