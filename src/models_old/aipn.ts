import Knex, * as knex from 'knex';

export class AipnModel {

  constructor() { }

  select_idpm(db: knex) {
    return db('idpm');
  }

  select_yearbudget(db:knex){
    return db('yearbudget')
  }

  async selete_raw_code(db: knex,code:any){
    let sql:any = `SELECT u.id,u.code,u.fullname,d.code as codedpm,d.name as namedpm FROM users u 
    INNER JOIN l_dprtm d on d.code = u.code 
    WHERE u.code = '${code}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }


async aipn_all_statusflg(db: Knex,ward: any, date_start:any,date_end: any){
 
    if (ward=='' || ward == 'null'){     
      let sql:any = `SELECT count(a.an) as countall,
      count(CASE WHEN a.status_flg = '' THEN '' END) AS status_null,
      count(CASE WHEN a.status_flg = 'A' THEN 'A' END) AS status_a,
      count(CASE WHEN a.status_flg = 'C' THEN 'C' END) AS status_c,
      count(CASE WHEN a.status_flg = 'D' THEN 'D' END) AS status_d
      
      from aipn_patient as a 
      
      where a.dchdate BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `SELECT count(a.an) as countall,
      count(CASE WHEN a.status_flg = '' THEN '' END) AS status_null,
      count(CASE WHEN a.status_flg = 'A' THEN 'A' END) AS status_a,
      count(CASE WHEN a.status_flg = 'C' THEN 'C' END) AS status_c,
      count(CASE WHEN a.status_flg = 'D' THEN 'D' END) AS status_d
      
      from aipn_patient as a 
      
      where a.dchdate between '${date_start}' and '${date_end}' and a.dischWard ='${ward}'`;
     let data:any = await db.raw(sql);
     return data[0];
     
    }
  }

  async aipn_statusflgN(db: Knex,ward: any, date_start:any,date_end: any){
 
    if (ward=='' || ward == 'null'){     
      let sql:any = `SELECT * from aipn_patient as a      
      where a.status_flg = '' and a.dchdate BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `SELECT * from aipn_patient as a      
      where a.status_flg = '' and a.dchdate BETWEEN '${date_start}' and '${date_end}' and a.dischWard ='${ward}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }

  }

  async aipn_statusflgA(db: Knex,ward: any, date_start:any,date_end: any){
 
    if (ward=='' || ward == 'null'){     
      let sql:any = `SELECT * from aipn_patient as a      
      where a.status_flg = 'A' and a.dchdate BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `SELECT * from aipn_patient as a      
      where a.status_flg = 'A' and a.dchdate BETWEEN '${date_start}' and '${date_end}' and a.dischWard ='${ward}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }

  }

  async aipn_statusflgC(db: Knex,ward: any, date_start:any,date_end: any){
 
    if (ward=='' || ward == 'null'){     
      let sql:any = `SELECT a.*,e.checkcode,c.cname,c.remark from aipn_patient as a 
      LEFT JOIN aipn_checkcode as e on a.an = e.an
      LEFT JOIN aipn_ccode as c on e.checkcode = c.ccode      
      where a.status_flg = 'C' and a.dchdate BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `SELECT a.*,e.checkcode,c.cname,c.remark from aipn_patient as a 
      LEFT JOIN aipn_checkcode as e on a.an = e.an
      LEFT JOIN aipn_ccode as c on e.checkcode = c.ccode     
      where a.status_flg = 'C' and a.dchdate BETWEEN '${date_start}' and '${date_end}' and a.dischWard ='${ward}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }

  }
  async aipn_statusflgD(db: Knex,ward: any, date_start:any,date_end: any){
 
    if (ward=='' || ward == 'null'){     
      let sql:any = `SELECT a.*,e.checkcode,c.cname,c.remark from aipn_patient as a 
      LEFT JOIN aipn_checkcode as e on a.an = e.an
      LEFT JOIN aipn_ccode as c on e.checkcode = c.ccode      
      where a.status_flg = 'D' and a.dchdate BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `SELECT a.*,e.checkcode,c.cname,c.remark from aipn_patient as a 
      LEFT JOIN aipn_checkcode as e on a.an = e.an
      LEFT JOIN aipn_ccode as c on e.checkcode = c.ccode      
      where a.status_flg = 'D' and a.dchdate BETWEEN '${date_start}' and '${date_end}' and a.dischWard ='${ward}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }

  }
}