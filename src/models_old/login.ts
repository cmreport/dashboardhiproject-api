import * as knex from 'knex';

export class LoginModel {

  constructor() { }

  login(db: knex, username: any, password: any) {
    return db('users')
      .select('id','cid', 'fullname','is_active')
      .where('cid', username)
      .andWhere('passwrd', password)
  }

}
