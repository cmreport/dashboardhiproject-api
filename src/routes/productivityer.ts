import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ProductivityerModel } from '../models/productivityer'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new ProductivityerModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Productivity ER Model' })
  })


  fastify.get('/select',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    try {
      const rs: any = await fromImport.select(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.post('/insert',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const datas: any = req.body;
    try {
      const rs: any = await fromImport.insert(db,datas)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.put('/update/:id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const datas: any = req.body;
    const id:any = req.params.id
    try {
      const rs: any = await fromImport.update(db,datas,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.delete('/delete/:id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const id:any = req.params.id

    try {
      const rs: any = await fromImport.delete(db,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_id(db,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_date',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const dstr: any = req.body.datestart;
    const dstp: any = req.body.dateend;
    try {
      const rs: any = await fromImport.select_date(db,dstr,dstp)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_date_proder',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const dstr: any = req.body.datestart;
    const dstp: any = req.body.dateend;
    try {
      const rs: any = await fromImport.select_date_proder(db,dstr,dstp)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

}
