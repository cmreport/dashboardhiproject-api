import Knex, * as knex from 'knex';

export class ExportF16Model {

    async select_lfund(db: knex) {
        let sql:any  =  `SELECT * from l_fund as s`;
         console.log('sql:',sql);   
         let data: any =  await db.raw(sql)
        return data[0];
      }

    async getPat(db: Knex, date_start: any,date_end: any) {
        
        let sql = `
        Select 
        s.hcode as HCODE
        ,p.hn as HN
        ,p.chwpart as CHANGWAT
        ,p.amppart as AMPHUR
        ,date_format(p.brthdate,'%Y%m%d') as DOB
        ,p.male as SEX
        ,p.mrtlst as MARRIAGE
        ,p.occptn as OCCUPA
        ,p.ntnlty as NATION
        ,p.pop_id as PERSON_ID
        ,concat(p.fname,' ',p.lname,',',if(p.pname <> '',p.pname,f.prename)) as NAMEPAT
        ,if(p.pname <> '',p.pname,f.prename)  as TITLE
        ,p.fname as FNAME
        ,p.lname as LNAME
        ,'1' as ID_TYPE
        from 
        hi.pt as p 
        inner join hi.ovst as o on p.hn = o.hn
        left join hi.ipt as i on o.an = i.an
        left join hi.pttype as t on p.pttype = t.pttype
        left join hi.l_prename as f on (CASE p.male 
            WHEN 1 THEN 
            IF( p.mrtlst < 6, IF( TIMESTAMPDIFF(year,p.brthdate,NOW() ) < 15, '001', '003' ),
            IF(  TIMESTAMPDIFF(year,p.brthdate,NOW()) < 20, '823', '831' )) 
            WHEN 2 THEN 
            IF( p.mrtlst = 1,IF(  TIMESTAMPDIFF(year,p.brthdate,NOW() ) < 15, '002', '004' ),
            IF ( p.mrtlst < 6, '005', '863' )) END) = f.prename_code
        join (select hcode from hi.setup limit 1) as s 
        where (date(o.vstdttm) between '${date_start}' and '${date_end}'   and o.an = 0  ) or (date(i.dchdate) between '${date_start}' and '${date_end}'   and o.an <> 0  )
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getIns(db: Knex, date_start: any,date_end: any) {
                let sql = `
        Select
        o.hn as HN
        ,t.inscl as INSCL
        ,c.hcode as HCODE
        ,t.stdcode as SUBTYPE
        ,p.pop_id as CID
        ,s.datein as DATEIN
        ,(case s.dateexp when null then '20250131' when '00000000' then '20250131' when 0 then '20250531' else '20241231' end) as DATEEXP
        ,s.hospmain as HOSPMAIN
        ,s.hospsub as HOSPSUB
        ,ifnull(i.govcode,'') as GOVCODE
        ,ifnull(i.govname,'') as GOVNAME
        ,ifnull(i.permitno,'') as PERMITNO
        ,ifnull(i.docno,'') as DOCNO
        ,if(t.inscl in ('OFC','LGO'),p.pop_id,'') as OWNRPID
        ,if(t.inscl in ('OFC','LGO'),concat(p.fname,' ',p.lname,',',p.pname),'') as OWNNAME
        ,if(o.an=0,'',o.an) as AN
        ,o.vn as SEQ
        ,'' as SUBINSCL
        ,'' as RELINSCL
        ,'' as HTYPE

        from
        hi.ovst as o
        inner join hi.pt as p on o.hn = p.hn
        left join hi.pttype as t on o.pttype = t.pttype
        left join hi.insure as s on o.hn = s.hn and o.pttype = s.pttype
        left join hi.inspt as i on o.vn=i.vn
        join (select hcode from hi.setup limit 1) as c 
        where date(o.vstdttm) between '${date_start}' and '${date_end}'  
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getOpd(db: Knex, date_start: any,date_end: any) {
        
        let sql = `
        Select
        o.hn as HN
        ,'' as CLINIC
        ,date_format(o.vstdttm,'%Y%m%d') as DATEOPD
        ,date_format(o.vstdttm,'%H%i') as TIMEOPD
        ,o.vn as SEQ
        ,'1' as UUC
        ,s.symptom as DETAIL
        ,o.tt as BTEMP
        ,o.sbp as SBP
        ,o.dbp as DBP
        ,o.pr as PR
        ,o.rr as RR
        ,'' as OPTYPE
        ,st.service as TYPEIN
        ,(case o.ovstost when '1' then '1' when '2' then '4' when '3' then '3' when '4' then '2' else '1' end) as TYPEOUT
        from
        hi.ovst as o
        left join hi.symptm as s on o.vn = s.vn
        left join hi.optriage as t on o.vn = t.vn
        left join hi.service_type as st on o.vn = st.vn
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getOrf(db: Knex, date_start: any,date_end: any) {
        // let info: any = req.body;

        let sql = `
        Select
        o.hn as HN
        ,date_format(o.vstdttm,'%Y%m%d') as DATEOPD
        ,c.f21_55 as CLINIC
        ,if(o.ovstost = 3 ,ro.rfrlct,ri.rfrlct) as REFER
        ,if(o.ovstost = 3 ,'2','1') as REFERTYPE
        ,o.vn as SEQ
        ,date_format(ro.vstdate,'%Y%m%d') as REFERDATE
        from
        hi.ovst as o
        left join hi.orfro as ro on o.vn = ro.vn
        left join hi.orfri as ri on o.vn = ri.vn
        left join hi.cln as c on o.cln = c.cln
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0 
        having REFER is not null
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getOdx(db: Knex, date_start: any,date_end: any) {
  
        let sql = `
        Select
        o.hn as HN
        ,date_format(o.vstdttm,'%Y%m%d') as DATEDX
        ,c.f21_55 as CLINIC
        ,d.icd10 as DIAG
        ,if(d.cnt = '1','1',if(SUBSTR(d.icd10,1,1) in ('V','W','X','Y'),'5','4')) as DXTYPE
        ,t.lcno as DRDX
        ,p.pop_id as PERSON_ID
        ,o.vn as SEQ
        from
        hi.ovstdx as d
        inner join hi.ovst as o on d.vn = o.vn
        inner join hi.pt as p on o.hn = p.hn
        left join hi.cln as c on o.cln = c.cln
        left join hi.dct as t on (
            case length(o.dct) when 4 then substr(o.dct,3,2) = t.dct when 5 then o.dct=t.lcno end 
        )
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0  
        Union
        Select distinct
        o.hn as HN
        ,date_format(o.vstdttm,'%Y%m%d') as DATEDX
        ,c.f21_55 as CLINIC
        ,replace(dx.icdda,'.','') as DIAG
        ,if(x.icd10 is null,'1','4') as DXTYPE
        ,t.lcno as DRDX
        ,p.pop_id as PERSON_ID
        ,o.vn as SEQ
        from
        hi.ovst as o 
        inner join hi.pt as p on o.hn = p.hn
        inner join hi.dt as d on o.vn = d.vn 
        inner join hi.dtdx as dx on d.dn = dx.dn
				left join hi.ovstdx as x on o.vn=x.vn
        left join hi.dentist as t on d.dnt = t.codedtt
        left join hi.cln as c on o.cln = c.cln
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0  
        
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getOop(db: Knex, date_start: any,date_end: any) {
        
        let sql = `
        Select
        o.hn as HN
        ,date_format(o.vstdttm,'%Y%m%d') as DATEOPD
        ,c.f21_55 as CLINIC
        ,pr.icd9cm as OPER
        ,t.lcno as DROPID
        ,p.pop_id as PERSON_ID
        ,o.vn as SEQ
        ,pr.charge as SERVICEPRICE
        from
        hi.ovst as o
        inner join hi.pt as p on o.hn = p.hn
        inner join hi.oprt as pr on o.vn = pr.vn
        left join hi.cln as c on o.cln = c.cln
        left join hi.dct as t on (
            case length(o.dct) when 4 then substr(o.dct,3,2) = t.dct when 5 then o.dct=t.lcno end
        )
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0  
        union 
        Select
        o.hn as HN
        ,date_format(o.vstdttm,'%Y%m%d') as DATEOPD
        ,c.f21_55 as CLINIC
        ,dx.dttx as OPER
        ,t.lcno as DROPID
        ,p.pop_id as PERSON_ID
        ,o.vn as SEQ
        ,dx.charge as SERVICEPRICE
        from
        hi.ovst as o 
        inner join hi.pt as p on o.hn = p.hn
        inner join hi.dt as d on o.vn = d.vn 
        inner join hi.dtdx as dx on d.dn = dx.dn
        left join hi.dentist as t on d.dnt = t.codedtt
        left join hi.cln as c on o.cln = c.cln
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getIpd(db: Knex, date_start: any,date_end: any) {
        
        let sql = `
        Select
        i.hn as HN
        ,i.an as AN
        ,date_format(i.rgtdate,'%Y%m%d') as DATEADM
        ,LPAD(i.rgttime,4,'0') as TIMEADM
        ,date_format(i.dchdate,'%Y%m%d') as DATEDSC
        ,LPAD(i.dchtime,4,'0') as TIMEDSC
        ,i.dchstts as DISCHS
        ,i.dchtype as DISCHT
        ,substr(d.op56,2,4) as WARDDSC
        ,i.dept as DEPT
        ,i.bw as ADM_W
        ,'1' as UUC
        ,'1' as SVCTYPE
        from
        hi.ipt as i
        left join hi.idpm as d on i.ward = d.idpm
        where date(i.dchdate) between '${date_start}' and '${date_end}' 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getIrf(db: Knex, date_start: any,date_end: any) {

        let sql = `
        Select
        i.an as AN
        ,if(i.dchtype = '4' ,ro.rfrlct,ri.rfrlct) as REFER
        ,if(i.dchtype = '4' ,'2','1') as REFERTYPE
        from
        hi.ipt as i
        left join hi.orfri as ri on i.vn = ri.vn
        left join hi.orfro as ro on i.an = ro.an
        where date(i.dchdate) between '${date_start}' and '${date_end}'
        having REFER is not null
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getIdx(db: Knex, date_start: any,date_end: any) {

        let sql = `
        select
        i.an as AN
        ,d.icd10 as DIAG
        ,d.itemno as DXTYPE
        ,t.lcno as DRDX
        from
        hi.ipt as i
        inner join iptdx as d on i.an = d.an
        left join hi.dct as t on (
            case length(d.dct) when 4 then substr(d.dct,3,2) = t.dct when 5 then d.dct=t.lcno end
        )
        where date(i.dchdate) between '${date_start}' and '${date_end}'
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getIop(db: Knex, date_start: any,date_end: any) {

        let sql = `
        Select
        i.an as AN
        ,pr.icd9cm as OPER
        ,'1' as OPTYPE
        ,t.lcno as DROPID
        ,date_format(pr.date,'%Y%m%d') as DATEIN
        ,LPAD(pr.time,4,'0') as TIMEIN
        ,'' as DATEOUT
        ,'' as TIMEOUT
        from
        hi.ipt as i
        inner join hi.ioprt as pr on i.an = pr.an
        left join hi.dct as t on (
            case length(pr.dct) when 4 then substr(pr.dct,3,2) = t.dct when 5 then pr.dct=t.lcno end
        )
        where date(i.dchdate) between '${date_start}' and '${date_end}'
        union 
        Select
        o.an as AN
        ,pr.icd9cm as OPER
        ,'1' as OPTYPE
        ,t.lcno as DROPID
        ,date_format(o.vstdttm,'%Y%m%d') as DATEIN
        ,date_format(o.vstdttm,'%H%i') as TIMEIN
        ,'' as DATEOUT
        ,'' as TIMEOUT
        from
        hi.ovst as o
        inner join hi.oprt as pr on o.vn = pr.vn
        left join hi.dct as t on (
            case length(o.dct) when 4 then substr(o.dct,3,2) = t.dct when 5 then o.dct=t.lcno end
        )
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an <> 0 
        union 
        Select
        o.an as AN
        ,dx.dttx as OPER
        ,'1' as OPTYPE
        ,t.lcno as DROPID
        ,date_format(o.vstdttm,'%Y%m%d') as DATEIN
        ,date_format(o.vstdttm,'%H%i') as TIMEIN
        ,'' as DATEOUT
        ,'' as TIMEOUT
        from
        hi.ovst as o 
        inner join hi.dt as d on o.vn = d.vn and o.an <> 0
        inner join hi.dtdx as dx on d.dn = dx.dn
        left join hi.dentist as t on d.dnt = t.codedtt
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an <> 0 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getCht(db: Knex, date_start: any,date_end: any) {

        let sql = `
        Select
        o.hn as HN
        ,o.an as AN
        ,date_format(o.vstdttm,'%Y%m%d') as DATE
        ,sum(i.rcptamt) as TOTAL
        ,ifnull(r.amnt,0.00) as PAID
        ,if(t.stdcode = 'A1','10',t.stdcode) as PTTYPE
        ,p.pop_id as PERSON_ID
        ,o.vn as SEQ
        ,'' as OPD_MEMO
        ,ifnull(r.rcptno,'') as INVOINCE_NO
        ,ifnull(concat(r.bookno,'/',r.pageno),'') as INVOINCE_LT
        from
        hi.ovst as o
        inner join hi.incoth as i on o.vn = i.vn
        inner join hi.pt as p on o.hn = p.hn
        left join hi.pttype as t on o.pttype = t.pttype
        left join hi.rcpt as r on o.vn = r.vn
        where date(o.vstdttm) between '${date_start}' and '${date_end}' and o.an = 0 
        group by o.vn
        union 
        Select
        o.hn as HN
        ,o.an as AN
        ,date_format(o.vstdttm,'%Y%m%d') as DATE
        ,sum(i.rcptamt) as TOTAL
        ,r.amnt as PAID
        ,if(t.stdcode = 'A1','10',t.stdcode) as PTTYPE
        ,p.pop_id as PERSON_ID
        ,o.vn as SEQ
        ,'' as OPD_MEMO
        ,r.rcptno as INVOINCE_NO
        ,concat(r.bookno,'/',r.pageno) as INVOINCE_LT
        from
        hi.ipt as ip
        inner join hi.ovst as o on ip.an = o.an
        inner join hi.incoth as i on o.vn = i.vn
        inner join hi.pt as p on o.hn = p.hn
        left join hi.pttype as t on o.pttype = t.pttype
        left join hi.rcpt as r on o.vn = r.vn
        where ip.dchdate between '${date_start}' and '${date_end}'  
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getCha(db: Knex, date_start: any,date_end: any) {

        let sql = `
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an = 0,'',o.an) as AN,
            date_format(r.date,'%Y%m%d') as DATE,
            c.chrgitem as CHRGITEM,
            sum(r.rcptamt) as AMOUNT,
            p.pop_id as PERSON_ID,
            r.vn as SEQ
        from 
        hi.incoth as r
        inner join hi.ovst as o on r.vn=o.vn
        inner join hi.pt as p on o.hn=p.hn
        left join hi.ipt as i on r.an=i.an
        inner join hi.income as c on r.income = c.costcenter
        where date(r.date) between '${date_start}' and '${date_end}' 
        group by r.vn,c.chrgitem`;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAer(db: Knex, date_start: any,date_end: any) {

        let sql = `
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an = 0,'',o.an) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            ifnull(k.claimCode,'') as AUTHAE,
            ifnull(e.sickdate,o.sickdate) as AEDATE,
            ifnull(e.sicktime,date_format(o.vstdttm,'%H%i')) as AETIME,
            if(aetype_ae = '01','V','') as AETYPE,
            r.rfrno as REFER_NO,
            ifnull(ri.rfrlct,'') as REFERMAINI,
            if(ri.rfrno is null,'',if(ri.rfrcs <= 7,'1100','1101')) as IREFTYPE,
            r.rfrlct as REFERMAINO,
            if(r.rfrcs <= 7,'1100','1101') as OREFTYPE,
            if(t.ucae is null or t.ucae = '','I',t.ucae) as UCAE,
            if(t.ucae in ('A','E'),'1','') as EMTYPE,
            cast(o.vn as char(20)) as SEQ,
            '' as AESTATUS,
            '' as DALERT,
            '' as TALERT
        from 
        hi.ovst as o 
        inner join hi.pt as p on o.hn=p.hn 
        inner join hi.orfro as r on o.vn=r.vn 
        left join hi.orfri as ri on o.vn=ri.vn
        left join hi.emergency as e on o.vn =e.vn
        left join hi.kios_pttype as k on o.vn=k.vn
        left join hi.optriage as t on o.vn=t.vn
        where 
        date(o.vstdttm) between '${date_start}' and '${date_end}' 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdp(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30001' as 'CODE',
            1 as QTY,
            1000.00 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            1000.00 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.ovstdx as x on o.vn=x.vn and x.icd10 = 'Z515'
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case length(o.dct) when 5 then d.lcno = o.dct when 4 then substr(o.dct,1,2)=d.dct end )
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        union
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30011' as 'CODE',
            1 as QTY,
            360.00 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            360.00 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            a.g as GRAVIDA,
            a.ga as GA_WEEK,
            '' as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30010' as 'CODE',
            1 as QTY,
            400.00 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            400.00 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            a.g as GRAVIDA,
            a.ga as GA_WEEK,
            '' as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            left join hi.cln as c on o.cln=c.cln
            inner join hi.oprt as r on o.vn=r.vn and r.icd9cm in ('8878')
            left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30012' as 'CODE',
            1 as QTY,
            400.00 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            400.00 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            a.g as GRAVIDA,
            a.ga as GA_WEEK,
            if(LOWER(substr(r.labresult,1,3)) = 'pos','28','29') as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            inner join hi.lbbk as l on l.labcode ='223' and l.vn=o.vn
            left join hi.labresult as r on l.ln=r.ln and lower(r.lab_code_local) = 'dcip'
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            group by o.vn
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30012' as 'CODE',
            1 as QTY,
            400.00 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            400.00 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            a.g as GRAVIDA,
            a.ga as GA_WEEK,
            if(LOWER(substr(r.labresult,1,3)) = 'pos','28','29') as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            inner join hi.lbbk as l1 on l1.labcode in ('002','001') and l1.vn=o.vn and l1.finish = '1'
            inner join hi.lbbk as l2 on l2.labcode ='068' and l2.vn=o.vn and l2.finish = '1'
            inner join hi.lbbk as l3 on l3.labcode ='070' and l3.vn=o.vn and l3.finish = '1'
            inner join hi.lbbk as l4 on l4.labcode ='126' and l4.vn=o.vn and l4.finish = '1'
            inner join hi.lbbk as l5 on l5.labcode ='229' and l5.vn=o.vn and l5.finish = '1'
            inner join hi.lbbk as l7 on l7.labcode ='061' and l7.vn=o.vn and l7.finish = '1'
            inner join hi.lbbk as l6 on l6.labcode in ('077','076') and l6.vn=o.vn and l6.finish = '1'
            left join hi.labresult as r on l4.ln=r.ln and r.lab_code_local != 'COMMENT'
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            group by o.vn
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30013' as 'CODE',
            1 as QTY,
            190.00 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            190.00 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            a.g as GRAVIDA,
            a.ga as GA_WEEK,
            '' as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            inner join hi.lbbk as l2 on l2.labcode ='068' and l2.vn=o.vn and l2.finish = '1'
            inner join hi.lbbk as l7 on l7.labcode ='061' and l7.vn=o.vn and l7.finish = '1'
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' and a.ga >= 28  
            group by o.vn
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            if(x.dttx = '2387010','30009','30008') as 'CODE',
            1 as QTY,
            if(x.dttx = '2387010',500.00,0) as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            if(x.dttx = '2387010',500.00,0) as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            a.g as GRAVIDA,
            a.ga as GA_WEEK,
            '' as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            inner join hi.dt as dt on o.vn = dt.vn 
            inner join hi.dtdx as x on dt.dn=x.dn and x.dttx in ('2330011','2387010') 
            left join hi.cln as c on o.cln=c.cln
            left join hi.dentist as d on dt.dnt=d.codedtt
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            (case 
                when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 34 then '12001'
                when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59 then '12002'
            end) as 'CODE',
            1 as QTY,
            (case 
                when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 34 then 100
                when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59 then 150
            end) as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            (case 
                when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 34 then 100
                when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59 then 150
            end) as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 59
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.screen as s on o.vn =s.vn
            inner join hi.visit2q as 2q on o.vn =2q.vn 
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
			'12003' as 'CODE',
            1 as QTY,
            40 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            40 as TOTAL,
            '' as QTYDAY,
            b.tmlt as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59
            inner join hi.lbbk as l on o.vn=l.vn and l.labcode in ('306','028')
            left join hi.lab as b on l.labcode=b.labcode
            inner join hi.screen as s on o.vn =s.vn
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
            group by o.vn
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '12004' as 'CODE',
            1 as QTY,
            160 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            160 as TOTAL,
            '' as QTYDAY,
            b.tmlt as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 45 and 70
            inner join hi.lbbk as l on o.vn=l.vn and l.labcode in ('521','036','035')
            left join hi.lab as b on l.labcode=b.labcode
            inner join hi.screen as s on o.vn =s.vn
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            group by o.vn
            union 
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
			'90005' as 'CODE',
            1 as QTY,
            60 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            60 as TOTAL,
            '' as QTYDAY,
            b.tmlt as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8
            inner join hi.lbbk as l on o.vn=l.vn and l.labcode = '212'
            left join hi.lab as b on l.labcode=b.labcode
            inner join hi.specialpp as s on o.vn=s.vn and s.ppcode in ('1B0060','1B0061')
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
            group by o.vn
            union 
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '3' as TYPE,
            (case 
                when f.fp in ('0207','0208') then 'FP002'
                when f.fp in ('0209','0210','0211','0212') then 'FP001'
            end) as 'CODE',
            1 as QTY,
            (case 
                when f.fp in ('0207','0208') then 2500
                when f.fp in ('0209','0210','0211','0212') then 800
            end) as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
            (case 
                when f.fp in ('0207','0208') then 2500
                when f.fp in ('0209','0210','0211','0212') then 800
            end) as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
            inner join hi.fp as f on o.vn=f.vn and f.fp between '0207' and '0212'
            inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z300','Z301','Z305','Z308')
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            union 
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
			'30014' as 'CODE',
            1 as QTY,
		    75 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
			75 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
            inner join hi.lbbk as l on l.vn=o.vn and l.labcode in ('073','135','137')
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
			'30015' as 'CODE',
            1 as QTY,
			150 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
			150 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
            inner join hi.service_type as s on o.vn=s.vn and s.service = 2
            inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z392')
            inner join hi.visitpostnatal as po on po.vn=o.vn
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
            group by o.vn
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
			'30016' as 'CODE',
            pd.qty as QTY,
			1.50 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
			135 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
            inner join hi.service_type as s on o.vn=s.vn and s.service = 2
            inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z392')
            inner join hi.visitpostnatal as po on po.vn=o.vn
            inner join hi.prsc as pr on o.vn=pr.vn
            inner join hi.prscdt as pd on pr.prscno=pd.prscno 
            inner join hi.meditem as m on pd.meditem = m.meditem and substr(m.stdcode,1,19) = '2011203200377262217'
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
            group by o.vn
            union
            select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '3' as TYPE,
			'TELMED' as 'CODE',
            1 as QTY,
			50 as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            0.00 as TOTCOPAY,
            '' as USE_STATUS,
			50 as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.pt as p on o.hn=p.hn 
            inner join hi.service_type as s on o.vn=s.vn and s.service = 5
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
            group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getLvd(db: Knex, req: any) {
        let info: any = req.body;

        let sql = ``;

        let data:any = await db.raw(sql);
        return data[0];
    }

    async getDru(db: Knex, req: any) {
        let info: any = req.body;


        let sql = `
        select 
            u.hcode as HCODE,
            cast(o.hn as char(20)) as HN,
            if(o.an = 0,'',o.an) as AN,
            c.f21_55 as CLINIC,
            p.pop_id as PERSON_ID,
            date_format(r.prscdate,'%Y%m%d') as DATE_SERV,
            d.meditem as DID,
            m.name as DNAME,
            d.qty as AMOUNT,
            m.price as DRUGPRICE,
            m.cost as DRUGCOST,
            m.stdcode as DIDSTD,
            m.pres_unt as UNIT,
            m.medunit as UNIT_PACK,
            r.vn as SEQ,
            '' as DRUGREMARK,
            '' as PA_NO,
            0.00 as TOTALPAY,
            if(d.qty < 40 ,if(o.an = 0 and d.rfndamt = 0,'2','1'),if(x.vn is null,'3','4')) as USE_STATUS,
            d.charge as TOTAL,
            ifnull(s.dosename,'') as SIGCODE,
            ifnull(concat(s.doseprn1,s.doseprn2),'') as SIGTEXT,
            ifnull(rx.cid,'') as PROVIDER
        from 
        hi.prsc as r
        inner join hi.ovst as o on r.vn=o.vn
        inner join hi.pt as p on o.hn=p.hn
        inner join hi.prscdt as d on r.prscno=d.prscno 
        inner join meditem as m on d.meditem=m.meditem and m.type in ('1','3')
        left join (select x.vn from hi.ovstdx as x inner join hi.ovst as o on x.vn=o.vn and date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' where (x.icd10 = 'I10' or x.icd10 = 'E789' or x.icd10 between 'E100' and 'E149' or x.icd10 between 'N181' and 'N189') group by x.vn) as x on o.vn =x.vn
        inner join hi.cln as c on o.cln=c.cln
        left join hi.medusage as s on d.medusage = s.dosecode
        left join hi.phrmcst as rx on r.pharmacist=rx.pmc
        join hi.setup as u
        left join hi.ipt as i on r.an=i.an
        where date(r.prscdate) between '${info.date_start}' and '${info.date_end}' and d.qty <> 0  
        `;

        let data:any = await db.raw(sql);
        return data[0];
    }

    async getLabfu(db: Knex, req: any) {
        let info: any = req.body;

        let sql = ``;

        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpPalliative(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30001' as 'CODE',
            '1' as QTY,
            '1000.00' as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            '0.00' as TOTCOPAY,
            '' as USE_STATUS,
            '1000.00' as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.ovstdx as x on o.vn=x.vn and x.icd10 = 'Z515'
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case length(o.dct) when 5 then d.lcno = o.dct when 4 then substr(o.dct,1,2)=d.dct end )
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  and o.an=0
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpAnc(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30011' as 'CODE',
            '1' as QTY,
            '360.00' as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            '0.00' as TOTCOPAY,
            '' as USE_STATUS,
            '360.00' as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            cast(a.g as char(2)) as GRAVIDA,
            cast(a.ga as char(2)) as GA_WEEK,
            '' as 'DCIP/E_screen',
            date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
            from 
            hi.ovst as o 
            inner join hi.pttype as t on o.pttype=t.pttype
            inner join hi.anc as a on o.vn=a.vn
            left join hi.cln as c on o.cln=c.cln
            left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
            where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpAncUltrasound(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '30010' as 'CODE',
        '1' as QTY,
        '400.00' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '400.00' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        cast(a.g as char(2)) as GRAVIDA,
        cast(a.ga as char(2)) as GA_WEEK,
        '' as 'DCIP/E_screen',
        date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.anc as a on o.vn=a.vn
        left join hi.cln as c on o.cln=c.cln
        inner join hi.oprt as r on o.vn=r.vn and r.icd9cm ='8878'
        left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpAncLab(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '30012' as 'CODE',
        '1' as QTY,
        '400.00' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '400.00' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        cast(a.g as char(2)) as GRAVIDA,
        cast(a.ga as char(2)) as GA_WEEK,
        if(LOWER(substr(r.labresult,1,3)) = 'pos','28','29') as 'DCIP/E_screen',
        date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.anc as a on o.vn=a.vn
        inner join hi.lbbk as l on l.labcode ='223' and l.vn=o.vn
        left join hi.labresult as r on l.ln=r.ln and lower(r.lab_code_local) = 'dcip'
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpAncLab2(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '30012' as 'CODE',
        '1' as QTY,
        '400.00' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '400.00' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        cast(a.g as char(2)) as GRAVIDA,
        cast(a.ga as char(2)) as GA_WEEK,
        if(LOWER(substr(r.labresult,1,3)) = 'pos','28','29') as 'DCIP/E_screen',
        date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.anc as a on o.vn=a.vn
        inner join hi.lbbk as l1 on l1.labcode in ('002','001','068','070','061','077','076','229','126') and l1.vn=o.vn and l1.finish = '1'
        left join hi.labresult as r on l1.ln=r.ln and r.lab_code_local != 'COMMENT' and r.labcode = '126'
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        group by o.vn
        having count(distinct l1.labcode) >= 7
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpAncLab3(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '30013' as 'CODE',
        '1' as QTY,
        '190.00' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '190.00' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        cast(a.g as char(2)) as GRAVIDA,
        cast(a.ga as char(2)) as GA_WEEK,
        '' as 'DCIP/E_screen',
        date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.anc as a on o.vn=a.vn
        inner join hi.lbbk as l2 on l2.labcode ='068' and l2.vn=o.vn and l2.finish = '1'
        inner join hi.lbbk as l7 on l7.labcode ='061' and l7.vn=o.vn and l7.finish = '1'
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case length(a.dct) when 5 then d.lcno = a.dct when 4 then substr(a.dct,1,2)=d.dct end )
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' and a.ga >= 28  
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpAncDental(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        if(x.dttx = '2387010','30009','30008') as 'CODE',
        '1' as QTY,
        if(x.dttx = '2387010','500.00','0') as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        if(x.dttx = '2387010','500.00','0') as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        cast(a.g as char(2)) as GRAVIDA,
        cast(a.ga as char(2)) as GA_WEEK,
        '' as 'DCIP/E_screen',
        date_format(a.lmp,'%Y%m%d') as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.anc as a on o.vn=a.vn
        inner join hi.dt as dt on o.vn = dt.vn 
        inner join hi.dtdx as x on dt.dn=x.dn and x.dttx in ('2330011','2387010') 
        left join hi.cln as c on o.cln=c.cln
        left join hi.dentist as d on dt.dnt=d.codedtt
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpScreening(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        (case 
            when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 34 then '12001'
            when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59 then '12002'
        end) as 'CODE',
        '1' as QTY,
        (case 
            when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 34 then '100'
            when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59 then '150'
        end) as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        (case 
            when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 34 then '100'
            when TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59 then '150'
        end) as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 15 and 59
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.visit2q as 2q on o.vn =2q.vn 
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpScreening2(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '12003' as 'CODE',
        '1' as QTY,
        '40' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '40' as TOTAL,
        '' as QTYDAY,
        b.tmlt as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 35 and 59
        inner join hi.lbbk as l on o.vn=l.vn and l.labcode in ('306','028')
        left join hi.lab as b on l.labcode=b.labcode
        inner join hi.visit2q as 2q on o.vn =2q.vn 
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpScreening3(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '12004' as 'CODE',
        '1' as QTY,
        '160' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '160' as TOTAL,
        '' as QTYDAY,
        b.tmlt as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) between 45 and 70
        inner join hi.lbbk as l on o.vn=l.vn and l.labcode in ('521','036','035')
        left join hi.lab as b on l.labcode=b.labcode
        inner join hi.visit2q as 2q on o.vn =2q.vn 
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpScreening4(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '90005' as 'CODE',
        '1' as QTY,
        '60' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '60' as TOTAL,
        '' as QTYDAY,
        b.tmlt as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8
        inner join hi.lbbk as l on o.vn=l.vn and l.labcode = '212'
        left join hi.lab as b on l.labcode=b.labcode
        inner join hi.specialpp as s on o.vn=s.vn and s.ppcode in ('1B0060','1B0061')
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpFamilyPlan(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '3' as TYPE,
        (case 
            when f.fp in ('0207','0208') then 'FP002'
            when f.fp in ('0209','0210','0211','0212') then 'FP001'
        end) as 'CODE',
        '1' as QTY,
        (case 
            when f.fp in ('0207','0208') then '2500'
            when f.fp in ('0209','0210','0211','0212') then '800'
        end) as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        (case 
            when f.fp in ('0207','0208') then '2500'
            when f.fp in ('0209','0210','0211','0212') then '800'
        end) as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
        inner join hi.fp as f on o.vn=f.vn and f.fp between '0207' and '0212'
        inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z300','Z301','Z305','Z308')
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpPregTest(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '30014' as 'CODE',
        '1' as QTY,
        '75' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '75' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
        inner join hi.lbbk as l on l.vn=o.vn and l.labcode in ('073','135','137')
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpPostnatal(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '30015' as 'CODE',
        '1' as QTY,
        '150' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '150' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
        inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z390','Z391','Z392')
        inner join hi.visitpostnatal as po on po.vn=o.vn
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpFerrus(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
            cast(o.hn as char(20)) as HN,
            if(o.an=0,'',cast(o.an as char(20))) as AN,
            date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
            '4' as TYPE,
            '30016' as 'CODE',
            pd.qty as QTY,
            '1.50' as RATE,
            cast(o.vn as char(20)) as SEQ,
            '' as CAGCODE,
            '' as DOSE,
            '' as CA_TYPE,
            '' as SERIALNO,
            '0.00' as TOTCOPAY,
            '' as USE_STATUS,
            '135' as TOTAL,
            '' as QTYDAY,
            '' as TMLTCODE,
            '' as STATUS1,
            '' as BI,
            c.f21_55 as CLINIC,
            '2' as ITEMSRC,
            d.cid as PROVIDER,
            '' as GRAVIDA,
            '' as GA_WEEK,
            '' as 'DCIP/E_screen',
            '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.visitpostnatal as po on po.vn=o.vn
        inner join hi.prsc as pr on o.vn=pr.vn
        inner join hi.prscdt as pd on pr.prscno=pd.prscno 
        inner join hi.meditem as m on pd.meditem = m.meditem and substr(m.stdcode,1,19) = '2011203200377262217'
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn and TIMESTAMPDIFF(year,p.brthdate,o.vstdttm) > 8 and p.male='2'
        inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z390','Z391','Z392')
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}' 
        group by o.vn
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpTelemedicine(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '3' as TYPE,
        'TELMED' as 'CODE',
        '1' as QTY,
        '50' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '50' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn 
        inner join hi.service_type as s on o.vn=s.vn and s.service = 5
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpFitTest(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        '4' as TYPE,
        '90005' as 'CODE',
        '1' as QTY,
        '60' as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        '60' as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        '' as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn 
        inner join hi.lbbk as l on o.vn=l.vn and l.labcode in ('212') and l.finish = '1'
        inner join hi.ovstdx as x on o.vn =x.vn and x.icd10 in ('Z121')
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }

    async getAdpOther(db: Knex, req: any) {
        let info: any = req.body;

        let sql = `
        select 
        cast(o.hn as char(20)) as HN,
        if(o.an=0,'',cast(o.an as char(20))) as AN,
        date_format(o.vstdttm,'%Y%m%d') as DATEOPD,
        l.adp_type as TYPE,
        l.adp_code as 'CODE',
        '1' as QTY,
        l.charge as RATE,
        cast(o.vn as char(20)) as SEQ,
        '' as CAGCODE,
        '' as DOSE,
        '' as CA_TYPE,
        '' as SERIALNO,
        '0.00' as TOTCOPAY,
        '' as USE_STATUS,
        l.charge as TOTAL,
        '' as QTYDAY,
        '' as TMLTCODE,
        '' as STATUS1,
        if(barindex is not null,barindex,'') as BI,
        c.f21_55 as CLINIC,
        '2' as ITEMSRC,
        d.cid as PROVIDER,
        '' as GRAVIDA,
        '' as GA_WEEK,
        '' as 'DCIP/E_screen',
        '' as LMP,'' as SP_ITEM
        from 
        hi.ovst as o 
        inner join hi.pttype as t on o.pttype=t.pttype
        inner join hi.pt as p on o.hn=p.hn 
        inner join hi.visititemoth as v on o.vn=v.vn 
        inner join hi.l_itemserviceoth as l on v.codeservid = l.codeservid and l.adp_code != '' and l.adp_code is not null
        left join hi.regisservicehome as r on o.vn=r.vn 
        left join hi.cln as c on o.cln=c.cln
        left join hi.dct as d on (case LENGTH(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        where date(o.vstdttm) between '${info.date_start}' and '${info.date_end}'  
        `;
        let data:any = await db.raw(sql);
        return data[0];
    }


}