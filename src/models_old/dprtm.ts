import Knex, * as knex from 'knex';

export class DprtmModel {

  constructor() { }

  select(db: knex) {
    return db('l_dprtm');
  }

  insert(db: knex,datas: any){
    return db('l_dprtm').insert(datas);
  }

  update(db: Knex,datas: any,id: any){
    return db('l_dprtm').update(datas).where('id',id);
  }

  delete(db: Knex,id: any){
    return db('l_dprtm').delete().where('id',id);
  }

  select_code(db: knex,code: any) {
    return db('l_dprtm').where('code',code);
  }
}
