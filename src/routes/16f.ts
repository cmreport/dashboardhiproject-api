import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ExportF16Model } from '../models/16f'
import { log } from 'console'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new ExportF16Model()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Export16 Model' })
  })

  fastify.post('/select_lfund', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;  
    try {
      const rs: any = await fromImport.select_lfund(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  });

  fastify.post('/getins',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    console.log('ins',info);
    
    try {
      let rs:any = await fromImport.getIns(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });
  fastify.post('/getpat',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getPat(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });
  fastify.post('/getopd',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getOpd(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });
  fastify.post('/getorf',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getOrf(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });
  fastify.post('/getodx',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getOdx(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });
  fastify.post('/getoop',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getOop(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });

  fastify.post('/getipd',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getIpd(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });
 
  fastify.post('/getirf',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getIrf(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });

  fastify.post('/getidx',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getIdx(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });

  fastify.post('/getiop',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getIop(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });

  fastify.post('/getcht',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getCht(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  

  });
  fastify.post('/getcha',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getCha(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });

  fastify.post('/getaer',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.getAer(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });

  
}
