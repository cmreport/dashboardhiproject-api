import Knex, * as knex from 'knex';

export class DashboardModel {

  constructor() { }

  select_cln(db: knex) {
    return db('cln');
  }

  select_yearbudget(db:knex){
    return db('yearbudget')
  }

  async selete_raw_code(db: knex,code:any){
    let sql:any = `SELECT u.id,u.code,u.fullname,d.code as codedpm,d.name as namedpm FROM users u 
    INNER JOIN l_dprtm d on d.code = u.code 
    WHERE u.code = '${code}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }


async service_opd_all(db: Knex, date_start:any,date_end: any,cln: any){
 
    if (cln=='' || cln == 'null'){     
      let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
      where  date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }else{
      let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
      where  o.cln ='${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
     let data:any = await db.raw(sql);
     return data[0];
    }

  }

  async service_opd_null(db: Knex, date_start:any,date_end: any,cln: any){
    if(cln=='' || cln == 'null'){
      let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
       where (o.ovstost = '' or o.ovstost = 0 ) and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
      let data:any = await db.raw(sql);
      return data[0];
    }else{
      let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
       where (o.ovstost = '' or o.ovstost = 0 ) and o.cln ='${cln}' and  date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
      let data:any = await db.raw(sql);
      return data[0];
    }
  
  }
  
async service_opd_home(db: Knex, date_start:any,date_end: any,cln: any){
  if(cln=='' || cln == 'null'){
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
     where o.ovstost = 1 and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }else{
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
     where o.ovstost = 1 and o.cln ='${cln}' and  date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }

}

async service_opd_dead(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln =='' || cln =='null'){
    let sql:any = `select count(distinct o.hn) as countHN from ovst as o
    where ovstost = 2 and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
   let data:any = await db.raw(sql);
   return data[0];

  }else{
    let sql:any = `select count(distinct o.hn) as countHN from ovst as o
    where o.ovstost = 2 and o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }

}

async service_opd_refer(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln =='' || cln =='null'){
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
    where o.ovstost = 3 and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }else{
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
    where o.ovstost = 3 and o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0]; 
  }
}

async service_opd_refer_in(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln =='' || cln =='null'){
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
    inner join orfri as i on o.vn = i.vn
    where  date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }else{
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
    inner join orfri as i on o.vn = i.vn
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0]; 
  }
}


async service_opd_admit(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln =='null') {
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
    where o.ovstost = 4 and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }else {
    let sql:any = `select count(distinct o.hn) as countHN,count(distinct o.vn) as countVN from ovst as o
    where o.ovstost = 4 and o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }

}

async getOpVisitByClinic(db: Knex, yearbudget: any,cln: any) {
  if (cln=='' || cln == 'null'){
    let data = await db('ovst as o')
    .innerJoin('cln as c', 'c.cln', 'o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('c.namecln as name')
    .groupBy('o.cln');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('cln as c', 'c.cln', 'o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select('c.namecln as name')
    .groupBy('o.cln');
    return data;
  }

}

async getOpVisitByAge(db: Knex, yearbudget: any,cln: any) {
  if (cln =='' || cln == 'null'){
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .count('o.vn as value')
      .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.vstdttm) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
    return data;
  }else{
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.cln',cln)
      .count('o.vn as value')
      .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.vstdttm) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
      return data;
  }

}

async getOpVisitByGender(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln =='null'){
    let data = await db('ovst as o')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }else{
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('male', 'pt.male', 'male.male')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.cln',cln)
      .count('o.vn as value')
      .select('male.namemale as name')
      .groupBy('pt.male');
      return data; 
  }
}

async getOpVisitByMonth(db: Knex, yearbudget: any,cln: any) {
  if(cln =='' || cln =='null'){
    let data = await db('ovst as o')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select(db.raw('month(o.vstdttm) as name'))
    .orderBy('o.vstdttm')
    .groupBy('name');
    return data;
  }else{
    let data = await db('ovst as o')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select(db.raw('month(o.vstdttm) as name'))
    .orderBy('o.vstdttm')
    .groupBy('name');
    return data;
  }

}
async getOpVisitByPttype(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }
}

async getVisit_IncothByMonth(db: Knex, yearbudget: any,cln: any) {
  if(cln =='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))           
    .select(db.raw('month(o.vstdttm) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.vstdttm')
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))            
    .andWhere('o.cln',cln)
    .select(db.raw('month(o.vstdttm) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.vstdttm')
    return data;
  }
}
//สรุปค่ารักษา + total visit
async getVisit_IncothByMonthPttype_Inscl(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))            
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }else {
    let data = await db('ovst as o')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))            
    .andWhere('o.cln',cln)
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }

}

//หาค่า max ในการแสดงกราฟ
async getMaxOpVisitbyMonth(db: Knex, yearbudget: any,cln: any){
  if(cln =='' || cln =='null'){
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.vstdttm) as name,count(o.vn) as value from ovst as o 
      where if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ?
      GROUP BY name
      order by o.vstdttm ) as x`
      let data:any = await db.raw(sql,[yearbudget]);
      return data[0];
  }else{
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.vstdttm) as name,count(o.vn) as value from ovst as o 
      where o.cln = ? and if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ?
      GROUP BY name
      order by o.vstdttm ) as x`
      let data:any = await db.raw(sql,[cln,yearbudget]);
      return data[0];
  }

}

}