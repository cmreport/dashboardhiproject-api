import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ProductivityModel } from '../models/productivity'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new ProductivityModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Productivity Model' })
  })


  fastify.get('/select',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    try {
      const rs: any = await fromImport.select(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.post('/insert',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const datas: any = req.body;
    try {
      const rs: any = await fromImport.insert(db,datas)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.put('/update/:id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const datas: any = req.body;
    const id:any = req.params.id
    try {
      const rs: any = await fromImport.update(db,datas,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.delete('/delete/:id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const id:any = req.params.id

    try {
      const rs: any = await fromImport.delete(db,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_id(db,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_date', { preValidation: [fastify.authenticate] },async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const dstr: any = req.body.datestart;
    const dstp: any = req.body.dateend;
    try {
      const rs: any = await fromImport.select_date(db,dstr,dstp)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_date_code', { preValidation: [fastify.authenticate] },async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const dstr: any = req.body.datestart;
    const dstp: any = req.body.dateend;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_date_code(db,dstr,dstp,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_dprtm',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_dprtm(db,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_pd_year',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const year: any = req.body.year;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_pd_year(db,year,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_pd_month',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const year: any = req.body.year;
    try {
      const rs: any = await fromImport.select_pd_month(db,year)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_pd_pttype', { preValidation: [fastify.authenticate] },async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const dstr: any = req.body.datestart;
    const dstp: any = req.body.dateend;
    try {
      const rs: any = await fromImport.select_pd_pttype(db,dstr,dstp)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

}
