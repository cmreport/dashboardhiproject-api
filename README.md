## API ##
# STEP 0

install typescript
```
npm i -g typescript
```

initial project

```
npm init -y
```

install fastify

```
npm i fastify -S
```

install typings
```
npm i -D typescript @types/node
```

initial typescript project
```
tsc --init 
```