import { log } from 'console';
import Knex, * as knex from 'knex';

export class StmIpOfcModel {

  constructor() { }

  select(db: knex) {
    return db('stm_ip_ofc');
  }

  async select_debt_account(db: knex) {
    let sql:any  =  `SELECT * from debt_account as s
        WHERE s.debt_type = 'IPD' `;
     console.log('sql:',sql);   
     let data: any =  await db.raw(sql)
    return data[0];
  }
  
  async select_debt_account_hi(db: knex) {
    let sql:any  =  `SELECT dept_name as debt_account_name,ip_code as debt_account_code from dept_code as s `;
     console.log('sql:',sql);   
     let data: any =  await db.raw(sql)
    return data[0];
  }

  async select_stm_ip_ofc(db: Knex,repno: number) {
    let sql:any  =  `SELECT s.*,if(length(substr(s.pid,5))= 9,concat(substr(s.pid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,(s.charge - s.total_summary) as diff from stm_ip_ofc as s
        WHERE s.repno = ? `;
     console.log('sql:',sql);
    let data: any = await db.raw(sql, [repno])
    
    return data[0];
         
  }  
  async select_stm_ip_ofc_stmno(db: Knex,stm_no: number) {
    let sql:any  =  `SELECT s.*,if(length(substr(s.pid,5))= 9,concat(substr(s.pid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,(s.charge - s.total_summary) as diff from stm_ip_ofc as s
        WHERE s.stm_no = ? order by repno asc`;
     console.log('sql:',sql);
    let data: any = await db.raw(sql, [stm_no])
    
    return data[0];
         
  }  

  async ipofcnull(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){
      let sql: any = `SELECT * from (
        SELECT
            d.hn,
            d.an,
            if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
            d.fullname,
            (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
            DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
            DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
          CASE
                
                WHEN dchdate = admitdate THEN
                1 ELSE DATEDIFF( dchdate, admitdate ) 
            END AS l_stay,
            d.charge AS charge,
            d.paid AS paid,
            d.debt AS debt,
            d.debt AS debt2,
            r.repno AS repno,
            s.repno AS stm_repno,
            r.errorcode AS error_code,
            v.code_name AS error_name,
            v.remarkdata AS remark_data,
            r.total_paid AS total_paid,
            s.adjrw,s.projcode,
          CASE
                
                WHEN r.total_paid IS NOT NULL THEN
                ( d.debt - r.total_paid ) ELSE NULL 
            END AS rep_diff,
          CASE
                
                WHEN ( d.debt - r.total_paid ) <= 0 THEN
                d.debt ELSE ( r.total_paid ) 
            END AS rep_diff2,
          CASE
                
                WHEN ( d.debt - r.total_paid ) <= 0 THEN
                ( r.total_paid - d.debt ) ELSE 0 
            END AS rest_debt,s.total_summary ,rc.receipt_no
          FROM
            debit_ip AS d
            LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
            LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
            LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
            LEFT JOIN receipt as rc on s.repno = rc.repno
          WHERE
             d.dchdate BETWEEN ?
            AND ?
          GROUP BY d.an 
          ORDER BY
            d.dchdate ) as x where repno is  null and stm_repno is null  `;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];
    }else {
      let sql: any = `SELECT * from (
        SELECT
            d.hn,
            d.an,
            if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
            d.fullname,
            (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
            DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
            DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
          CASE
                
                WHEN dchdate = admitdate THEN
                1 ELSE DATEDIFF( dchdate, admitdate ) 
            END AS l_stay,
            d.charge AS charge,
            d.paid AS paid,
            d.debt AS debt,
            d.debt AS debt2,
            r.repno AS repno,
            s.repno AS stm_repno,
            r.errorcode AS error_code,
            v.code_name AS error_name,
            v.remarkdata AS remark_data,
            r.total_paid AS total_paid,
            s.adjrw,s.projcode,
          CASE
                
                WHEN r.total_paid IS NOT NULL THEN
                ( d.debt - r.total_paid ) ELSE NULL 
            END AS rep_diff,
          CASE
                
                WHEN ( d.debt - r.total_paid ) <= 0 THEN
                d.debt ELSE ( r.total_paid ) 
            END AS rep_diff2,
          CASE
                
                WHEN ( d.debt - r.total_paid ) <= 0 THEN
                ( r.total_paid - d.debt ) ELSE 0 
            END AS rest_debt,s.total_summary ,rc.receipt_no
          FROM
            debit_ip AS d
            LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
            LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
            LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
            LEFT JOIN receipt as rc on s.repno = rc.repno
          WHERE
            d.acc_code = ? 
            AND d.dchdate BETWEEN ?
            AND ?
          GROUP BY d.an 
          ORDER BY
            d.dchdate ) as x where repno is  null and stm_repno is null   `;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }
   
}
//ดำเนินการเสร็จแล้วมี statement
async ipofcnotnull(db: Knex, accType: any, startDate: any, endDate: any) {
  if (accType=='' || accType == 'null'){
    let sql: any = `SELECT *   from (
      SELECT	d.hn,
      d.an,
      if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
      d.fullname,
      (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
      DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
      DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
    CASE
          
          WHEN dchdate = admitdate THEN
          1 ELSE DATEDIFF( dchdate, admitdate ) 
      END AS l_stay,
      d.charge AS charge,
      d.paid AS paid,
      d.debt AS debt,
      d.debt AS debt2,
      r.repno as repno,
      s.repno AS stm_repno,
      r.errorcode AS error_code,
      v.code_name AS error_name,
      v.remarkdata AS remark_data,
      r.total_paid AS total_paid,
    s.adjrw,s.projcode,
    CASE
          
          WHEN r.total_paid IS NOT NULL THEN
          ( d.debt - r.total_paid ) ELSE NULL 
      END AS rep_diff,
    CASE
          
          WHEN ( d.debt - r.total_paid ) <= 0 THEN
          d.debt ELSE ( r.total_paid ) 
      END AS rep_diff2,
    CASE
          
          WHEN ( d.debt - r.total_paid ) <= 0 THEN
          ( r.total_paid - d.debt ) ELSE 0 
      END AS rest_debt,s.total_summary,rc.receipt_no
    FROM
      debit_ip AS d
      LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
      LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
      LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
      LEFT JOIN receipt as rc on s.repno = rc.repno
    WHERE
     d.dchdate BETWEEN ?
      AND ?  and (r.errorcode = '-' or r.errorcode is null)
   group by d.an
    ORDER BY
      d.dchdate ) as x where  stm_repno is not null `;
  let data: any = await db.raw(sql, [startDate, endDate])
  return data[0]; 
  }else {
    let sql: any = `SELECT *   from (
      SELECT	d.hn,
      d.an,
      if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
      d.fullname,
      (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
      DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
      DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
    CASE
          
          WHEN dchdate = admitdate THEN
          1 ELSE DATEDIFF( dchdate, admitdate ) 
      END AS l_stay,
      d.charge AS charge,
      d.paid AS paid,
      d.debt AS debt,
      d.debt AS debt2,
      r.repno as repno,
      s.repno AS stm_repno,
      r.errorcode AS error_code,
      v.code_name AS error_name,
      v.remarkdata AS remark_data,
      r.total_paid AS total_paid,
    s.adjrw,s.projcode,
    CASE
          
          WHEN r.total_paid IS NOT NULL THEN
          ( d.debt - r.total_paid ) ELSE NULL 
      END AS rep_diff,
    CASE
          
          WHEN ( d.debt - r.total_paid ) <= 0 THEN
          d.debt ELSE ( r.total_paid ) 
      END AS rep_diff2,
    CASE
          
          WHEN ( d.debt - r.total_paid ) <= 0 THEN
          ( r.total_paid - d.debt ) ELSE 0 
      END AS rest_debt,s.total_summary,rc.receipt_no
    FROM
      debit_ip AS d
      LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
      LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
      LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
      LEFT JOIN receipt as rc on s.repno = rc.repno
    WHERE
      d.acc_code = ?
      AND d.dchdate BETWEEN ?
      AND ?  and (r.errorcode = '-' or r.errorcode is null)
   group by d.an
    ORDER BY
      d.dchdate ) as x where stm_repno is not null `;
  let data: any = await db.raw(sql, [accType, startDate, endDate])
  return data[0]; 
  }
    
}
//total รอดำเนินการ
async ipofcaccnull(db: Knex, accType: any, startDate: any, endDate: any) {
  if (accType=='' || accType == 'null'){
    let sql: any = `SELECT COUNT( an) AS count_an , 
    SUM(debt ) AS  sum_debt   from (
  SELECT
  d.hn,
  d.an,
  if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
  d.fullname,
  (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
  DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
  DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
  CASE
    
    WHEN dchdate = admitdate THEN
    1 ELSE DATEDIFF( dchdate, admitdate ) 
  END AS l_stay,
  d.charge AS charge,
  d.paid AS paid,
  d.debt AS debt,
  d.debt AS debt2,
  r.repno as repno,
  s.repno AS stm_repno,
  r.errorcode AS error_code,
  v.code_name AS error_name,
  v.remarkdata AS remark_data,
  r.total_paid AS total_paid,
    s.adjrw,
  CASE
    
    WHEN r.total_paid IS NOT NULL THEN
    ( d.debt - r.total_paid ) ELSE NULL 
  END AS rep_diff,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    d.debt ELSE ( r.total_paid ) 
  END AS rep_diff2,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    ( r.total_paid - d.debt ) ELSE 0 
  END AS rest_debt,s.total_summary ,rc.receipt_no
  FROM
  debit_ip AS d
  LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
  LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
  LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
  LEFT JOIN receipt as rc on s.repno = rc.repno
  WHERE
   d.dchdate BETWEEN ? 
  AND ?
  GROUP BY d.an 
  ORDER BY
  d.dchdate ) as x where repno is  null and stm_repno is null `;
    let data: any = await db.raw(sql, [startDate, endDate])
    return data[0];
  }else {
    let sql: any = `SELECT COUNT( an) AS count_an , 
    SUM(debt ) AS  sum_debt   from (
  SELECT
  d.hn,
  d.an,
  if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
  d.fullname,
  (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
  DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
  DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
  CASE
    
    WHEN dchdate = admitdate THEN
    1 ELSE DATEDIFF( dchdate, admitdate ) 
  END AS l_stay,
  d.charge AS charge,
  d.paid AS paid,
  d.debt AS debt,
  d.debt AS debt2,
  r.repno as repno,
  s.repno AS stm_repno,
  r.errorcode AS error_code,
  v.code_name AS error_name,
  v.remarkdata AS remark_data,
  r.total_paid AS total_paid,
    s.adjrw,
  CASE
    
    WHEN r.total_paid IS NOT NULL THEN
    ( d.debt - r.total_paid ) ELSE NULL 
  END AS rep_diff,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    d.debt ELSE ( r.total_paid ) 
  END AS rep_diff2,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    ( r.total_paid - d.debt ) ELSE 0 
  END AS rest_debt,s.total_summary ,rc.receipt_no
  FROM
  debit_ip AS d
  LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
  LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
  LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
  LEFT JOIN receipt as rc on s.repno = rc.repno
  WHERE
  d.acc_code = ? 
  AND d.dchdate BETWEEN ? 
  AND ?
  GROUP BY d.an 
  ORDER BY
  d.dchdate ) as x where repno is null and stm_repno is null `;
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];
  }
   
  }
//รอดำเนินการไม่มี repno และ statement.repno
async ipofcnotnull_stm(db: Knex, accType: any, startDate: any, endDate: any) {
if (accType=='' || accType == 'null'){
  let sql: any = `SELECT COUNT( an) AS count_an , 
  SUM(debt ) AS  sum_debt   from (
SELECT
d.hn,
d.an,
if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
d.fullname,
(SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
CASE
  
  WHEN dchdate = admitdate THEN
  1 ELSE DATEDIFF( dchdate, admitdate ) 
END AS l_stay,
d.charge AS charge,
d.paid AS paid,
d.debt AS debt,
d.debt AS debt2,
r.repno as repno,
s.repno AS stm_repno,
r.errorcode AS error_code,
v.code_name AS error_name,
v.remarkdata AS remark_data,
r.total_paid AS total_paid,
  s.adjrw,
CASE
  
  WHEN r.total_paid IS NOT NULL THEN
  ( d.debt - r.total_paid ) ELSE NULL 
END AS rep_diff,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  d.debt ELSE ( r.total_paid ) 
END AS rep_diff2,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  ( r.total_paid - d.debt ) ELSE 0 
END AS rest_debt,s.total_summary ,rc.receipt_no
FROM
debit_ip AS d
LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
LEFT JOIN receipt as rc on s.repno = rc.repno
WHERE
 d.dchdate BETWEEN ? 
AND ?
GROUP BY d.an 
ORDER BY
d.dchdate ) as x where repno is not null and stm_repno is null `;
  let data: any = await db.raw(sql, [startDate, endDate])
  return data[0];
}else {
  let sql: any = `SELECT COUNT( an) AS count_an , 
  SUM(debt ) AS  sum_debt   from (
SELECT
d.hn,
d.an,
if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
d.fullname,
(SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
CASE
  
  WHEN dchdate = admitdate THEN
  1 ELSE DATEDIFF( dchdate, admitdate ) 
END AS l_stay,
d.charge AS charge,
d.paid AS paid,
d.debt AS debt,
d.debt AS debt2,
r.repno as repno,
s.repno AS stm_repno,
r.errorcode AS error_code,
v.code_name AS error_name,
v.remarkdata AS remark_data,
r.total_paid AS total_paid,
  s.adjrw,
CASE
  
  WHEN r.total_paid IS NOT NULL THEN
  ( d.debt - r.total_paid ) ELSE NULL 
END AS rep_diff,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  d.debt ELSE ( r.total_paid ) 
END AS rep_diff2,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  ( r.total_paid - d.debt ) ELSE 0 
END AS rest_debt,s.total_summary ,rc.receipt_no
FROM
debit_ip AS d
LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
LEFT JOIN receipt as rc on s.repno = rc.repno
WHERE
d.acc_code = ? 
AND d.dchdate BETWEEN ? 
AND ?
GROUP BY d.an 
ORDER BY
d.dchdate ) as x where repno is not null and stm_repno is null `;
  let data: any = await db.raw(sql, [accType, startDate, endDate])
  return data[0];
}
 
}

async ipofcaccnotnull_stm(db: Knex, accType: any, startDate: any, endDate: any) {
  if (accType=='' || accType == 'null'){
    let sql: any = `SELECT *   from (
  SELECT
  d.hn,
  d.an,
  if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
  d.fullname,
  (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
  DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
  DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
  CASE
    
    WHEN dchdate = admitdate THEN
    1 ELSE DATEDIFF( dchdate, admitdate ) 
  END AS l_stay,
  d.charge AS charge,
  d.paid AS paid,
  d.debt AS debt,
  d.debt AS debt2,
  r.repno as repno,
  s.repno AS stm_repno,
  r.errorcode AS error_code,
  v.code_name AS error_name,
  v.remarkdata AS remark_data,
  r.total_paid AS total_paid,
    s.adjrw,
  CASE
    
    WHEN r.total_paid IS NOT NULL THEN
    ( d.debt - r.total_paid ) ELSE NULL 
  END AS rep_diff,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    d.debt ELSE ( r.total_paid ) 
  END AS rep_diff2,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    ( r.total_paid - d.debt ) ELSE 0 
  END AS rest_debt,s.total_summary ,rc.receipt_no
  FROM
  debit_ip AS d
  LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
  LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
  LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
  LEFT JOIN receipt as rc on s.repno = rc.repno
  WHERE
   d.dchdate BETWEEN ? 
  AND ?
  GROUP BY d.an 
  ORDER BY
  d.dchdate ) as x where repno is not null and stm_repno is null `;
    let data: any = await db.raw(sql, [startDate, endDate])
    return data[0];
  }else {
    let sql: any = `SELECT *   from (
  SELECT
  d.hn,
  d.an,
  if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
  d.fullname,
  (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
  DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
  DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
  CASE
    
    WHEN dchdate = admitdate THEN
    1 ELSE DATEDIFF( dchdate, admitdate ) 
  END AS l_stay,
  d.charge AS charge,
  d.paid AS paid,
  d.debt AS debt,
  d.debt AS debt2,
  r.repno as repno,
  s.repno AS stm_repno,
  r.errorcode AS error_code,
  v.code_name AS error_name,
  v.remarkdata AS remark_data,
  r.total_paid AS total_paid,
    s.adjrw,
  CASE
    
    WHEN r.total_paid IS NOT NULL THEN
    ( d.debt - r.total_paid ) ELSE NULL 
  END AS rep_diff,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    d.debt ELSE ( r.total_paid ) 
  END AS rep_diff2,
  CASE
    
    WHEN ( d.debt - r.total_paid ) <= 0 THEN
    ( r.total_paid - d.debt ) ELSE 0 
  END AS rest_debt,s.total_summary ,rc.receipt_no
  FROM
  debit_ip AS d
  LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
  LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
  LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
  LEFT JOIN receipt as rc on s.repno = rc.repno
  WHERE
  d.acc_code = ? 
  AND d.dchdate BETWEEN ? 
  AND ?
  GROUP BY d.an 
  ORDER BY
  d.dchdate ) as x where repno is not null and stm_repno is null `;
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];
  }
   
  }

//total ดำเนินการเสร็จ
async ipofcaccnotnull(db: Knex, accType: any, startDate: any, endDate: any) {
if (accType=='' || accType == 'null'){
  let sql: any = `SELECT COUNT( an) AS all_notnullcase , 
  SUM(debt ) AS  debit_notnull, 
  SUM(total_summary ) AS recieve, 
  SUM(debt-total_summary) AS sum_diff , 
  SUM(CASE WHEN (debt-total_summary) > 0 THEN (debt-total_summary) ELSE 0 END) AS diffgain ,  
  SUM(CASE WHEN (debt-total_summary) < 0 THEN (debt-total_summary) ELSE 0 END) AS diffloss   from (
  SELECT	d.hn,
d.an,
if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
d.fullname,
(SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
CASE
  
  WHEN dchdate = admitdate THEN
  1 ELSE DATEDIFF( dchdate, admitdate ) 
END AS l_stay,
d.charge AS charge,
d.paid AS paid,
d.debt AS debt,
d.debt AS debt2,
s.repno AS repno,
r.errorcode AS error_code,
v.code_name AS error_name,
v.remarkdata AS remark_data,
r.total_paid AS total_paid,
  s.adjrw,
CASE
  
  WHEN r.total_paid IS NOT NULL THEN
  ( d.debt - r.total_paid ) ELSE NULL 
END AS rep_diff,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  d.debt ELSE ( r.total_paid ) 
END AS rep_diff2,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  ( r.total_paid - d.debt ) ELSE 0 
END AS rest_debt,s.total_summary ,rc.receipt_no
FROM
debit_ip AS d
LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
LEFT JOIN receipt as rc on s.repno = rc.repno
WHERE
 d.dchdate BETWEEN ? 
    AND ? and (r.errorcode = '-' or r.errorcode is null)
group by d.an
ORDER BY
d.dchdate ) as x where repno is not null `;
let data: any = await db.raw(sql, [startDate, endDate])
return data[0];
}
else {
  let sql: any = `SELECT COUNT( an) AS all_notnullcase , 
  SUM(debt ) AS  debit_notnull, 
  SUM(total_summary ) AS recieve, 
  SUM(debt-total_summary) AS sum_diff , 
  SUM(CASE WHEN (debt-total_summary) > 0 THEN (debt-total_summary) ELSE 0 END) AS diffgain ,  
  SUM(CASE WHEN (debt-total_summary) < 0 THEN (debt-total_summary) ELSE 0 END) AS diffloss   from (
  SELECT	d.hn,
d.an,
if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
d.fullname,
(SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
CASE
  
  WHEN dchdate = admitdate THEN
  1 ELSE DATEDIFF( dchdate, admitdate ) 
END AS l_stay,
d.charge AS charge,
d.paid AS paid,
d.debt AS debt,
d.debt AS debt2,
s.repno AS repno,
r.errorcode AS error_code,
v.code_name AS error_name,
v.remarkdata AS remark_data,
r.total_paid AS total_paid,
  s.adjrw,
CASE
  
  WHEN r.total_paid IS NOT NULL THEN
  ( d.debt - r.total_paid ) ELSE NULL 
END AS rep_diff,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  d.debt ELSE ( r.total_paid ) 
END AS rep_diff2,
CASE
  
  WHEN ( d.debt - r.total_paid ) <= 0 THEN
  ( r.total_paid - d.debt ) ELSE 0 
END AS rest_debt,s.total_summary ,rc.receipt_no
FROM
debit_ip AS d
LEFT JOIN stm_ip_ofc AS s ON d.an = s.an
LEFT JOIN rep_ip_ofc AS r ON d.an = r.an
LEFT JOIN l_validatedata AS v ON r.errorcode = v.code_id 
LEFT JOIN receipt as rc on s.repno = rc.repno
WHERE
d.acc_code = ? 
AND d.dchdate BETWEEN ? 
    AND ? and (r.errorcode = '-' or r.errorcode is null)
group by d.an
ORDER BY
d.dchdate ) as x where repno is not null `;
let data: any = await db.raw(sql, [accType, startDate, endDate])
return data[0];
}


}

async ipofcaccbydate(db: Knex, accType: any, startDate: any, endDate: any) {
  if (accType=='' || accType == 'null'){
    let sql: any = `SELECT 
    d.dchdate
    , COUNT(d.an)  AS allcase 
    , SUM(d.debt) AS  debit 
    , SUM(CASE WHEN s.repno IS NULL THEN 1 ELSE 0 END)  AS nullcase,
    SUM(CASE WHEN s.repno IS  NULL THEN d.debt ELSE 0 END)  AS nulldebit ,
    SUM(CASE WHEN s.repno IS NOT NULL THEN 1 ELSE 0 END)  AS notnullcase
    ,SUM(CASE WHEN s.repno IS NOT NULL THEN d.debt ELSE 0 END)  AS notnulldebit,
    SUM(s.total_summary) AS recieve 
    ,SUM(CASE WHEN s.repno IS NOT NULL THEN d.debt ELSE 0 END)-SUM(s.total_summary) AS diff 
     FROM debit_ip AS d LEFT OUTER JOIN stm_ip_ofc AS s ON d.an = s.an  
    WHERE dchdate BETWEEN ? AND ?  GROUP BY d.dchdate`;
    let data: any = await db.raw(sql, [startDate, endDate])
    return data[0];
  }else {
    let sql: any = `SELECT 
    d.dchdate
    , COUNT(d.an)  AS allcase 
    , SUM(d.debt) AS  debit 
    , SUM(CASE WHEN s.repno IS NULL THEN 1 ELSE 0 END)  AS nullcase,
    SUM(CASE WHEN s.repno IS  NULL THEN d.debt ELSE 0 END)  AS nulldebit ,
    SUM(CASE WHEN s.repno IS NOT NULL THEN 1 ELSE 0 END)  AS notnullcase
    ,SUM(CASE WHEN s.repno IS NOT NULL THEN d.debt ELSE 0 END)  AS notnulldebit,
    SUM(s.total_summary) AS recieve 
    ,SUM(CASE WHEN s.repno IS NOT NULL THEN d.debt ELSE 0 END)-SUM(s.total_summary) AS diff 
     FROM debit_ip AS d LEFT OUTER JOIN stm_ip_ofc AS s ON d.an = s.an  
    WHERE d.acc_code = ?
    AND dchdate BETWEEN ? AND ?  GROUP BY d.dchdate`;
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];
}

}

async stm_ip_ofc_sum(db: Knex, repno: any) {
    let sql: any = `SELECT
    stm.repno,
    COUNT(stm.an) AS "all_case",
    SUM(CAST(stm.charge AS DECIMAL)) AS "debt",
    SUM(CAST(stm.total_summary AS DECIMAL)) AS "receive",
    SUM(stm.diff) AS "sum_diff",
    SUM(CASE WHEN stm.diff > 0 THEN stm.diff ELSE 0 END) AS diffgain,
    SUM(CASE WHEN stm.diff < 0 THEN stm.diff ELSE 0 END) AS diffloss
FROM
    (
    SELECT
        sd.repno,
        sd.an,
        CAST(sd.charge AS DECIMAL) AS charge,
        CAST(sd.total_summary AS DECIMAL) AS total_summary,
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_ip_ofc sd
    WHERE
        sd.repno = ?
    ORDER BY
        sd.date_dch
    ) stm
GROUP BY
    stm.repno`;
    let data: any = await db.raw(sql, [repno])
    return data[0];
}

async stm_ip_ofc_sum_stmno(db: Knex, stm_no: any) {
  let sql: any = `SELECT
  stm.stm_no,
  COUNT(stm.an) AS "all_case",
  SUM(CAST(stm.charge AS DECIMAL)) AS "debt",
  SUM(CAST(stm.total_summary AS DECIMAL)) AS "receive",
  SUM(stm.diff) AS "sum_diff",
  SUM(CASE WHEN stm.diff > 0 THEN stm.diff ELSE 0 END) AS diffgain,
  SUM(CASE WHEN stm.diff < 0 THEN stm.diff ELSE 0 END) AS diffloss
FROM
  (
  SELECT
      sd.stm_no,
      sd.an,
      CAST(sd.charge AS DECIMAL) AS charge,
      CAST(sd.total_summary AS DECIMAL) AS total_summary,
      (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
  FROM
      stm_ip_ofc sd
  WHERE
      sd.stm_no = ?
  ORDER BY
      sd.date_dch
  ) stm
GROUP BY
  stm.stm_no`;
  let data: any = await db.raw(sql, [stm_no])
  return data[0];
}

async stm_ip_ofc_detail(db: Knex, repno: any) {
    let sql: any = `SELECT
    stm.*
FROM
    (
    SELECT
         sd.*,if(length(substr(sd.pid,5))= 9,concat(substr(sd.pid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,         
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_ip_ofc sd
    WHERE
        sd.repno = ?
     ORDER BY
        sd.date_dch
    ) stm`;
let data: any = await db.raw(sql, [repno])
    return data[0];
}

async stm_countan(db:Knex,accType:any,startDate:any,endDate:any){
  if (accType=='' || accType == 'null'){
    let sql:any =` SELECT 
    COUNT( DISTINCT d.an) AS all_ip 
    FROM debit_ip AS d 
    WHERE  d.dchdate BETWEEN ? 
	    AND ? `
    let data: any = await db.raw(sql, [startDate, endDate])
    return data[0];     
  }
  else {
    let sql:any =` SELECT 
    COUNT( DISTINCT d.an) AS all_ip 
    FROM debit_ip AS d 
    WHERE d.acc_code = ?
	AND d.dchdate BETWEEN ? 
	    AND ? `
    let data: any = await db.raw(sql, [accType, startDate, endDate])
    return data[0];  
  }
}
async toperrorcode(db: Knex, accType: any, startDate: any, endDate: any) {
  if (accType=='' || accType == 'null'){
    let sql: any = `SELECT error_code,
    error_name,
    count(error_code) as counterror from (
      SELECT
          d.hn,
          d.an,
          if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
          d.fullname,
          (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
          DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
          DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
        CASE
              
              WHEN dchdate = admitdate THEN
              1 ELSE DATEDIFF( dchdate, admitdate ) 
          END AS l_stay,
          d.charge AS charge,
          d.paid AS paid,
          d.debt AS debt,
          d.debt AS debt2,
          r.repno AS repno,
          r.error_code AS error_code,
          v.code_name AS error_name,
          v.remarkdata AS remark_data
        FROM
          debit_ip AS d
          LEFT JOIN rep_ip_errorcode AS r ON d.an = r.an
          LEFT JOIN l_validatedata AS v ON r.error_code = v.code_id 
        WHERE r.pttype = 'OFC' and d.dchdate BETWEEN ?
          AND ?
        
        ORDER BY
          d.dchdate ) as x where repno is null or error_code != '-' GROUP BY error_code order by counterror desc  `;
      let data: any = await db.raw(sql, [startDate, endDate])
      return data[0];
  }else {
    let sql: any = `SELECT error_code,
    error_name,
    count(error_code) as counterror  from (
      SELECT
          d.hn,
          d.an,
          if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
          d.fullname,
          (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
          DATE_FORMAT( d.admitdate, '%Y-%m-%d' ) AS admitdate,
          DATE_FORMAT( d.dchdate, '%Y-%m-%d' ) AS dchdate,
        CASE
              
              WHEN dchdate = admitdate THEN
              1 ELSE DATEDIFF( dchdate, admitdate ) 
          END AS l_stay,
          d.charge AS charge,
          d.paid AS paid,
          d.debt AS debt,
          d.debt AS debt2,
          r.repno AS repno,
          r.error_code AS error_code,
          v.code_name AS error_name,
          v.remarkdata AS remark_data
        FROM
          debit_ip AS d
          LEFT JOIN rep_ip_errorcode AS r ON d.an = r.an
          LEFT JOIN l_validatedata AS v ON r.error_code = v.code_id 
        WHERE r.pttype = 'OFC' and 
          d.acc_code = ? 
          AND d.dchdate BETWEEN ?
          AND ?
         
        ORDER BY
          d.dchdate ) as x where repno is null or error_code != '-' GROUP BY error_code order by counterror desc `;
      let data: any = await db.raw(sql, [accType, startDate, endDate])
      return data[0];
  }


}   
}